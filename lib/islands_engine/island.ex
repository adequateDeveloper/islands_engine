defmodule IslandsEngine.Island do
  @moduledoc """
  Islands are made up of groups of coordinates.

  They come in five different shapes - :atoll, :dot, :l_shape, :s_shape, and
  :square. For all islands, the starting coordinate is the upper left corner.

  Players position them on their board, and their opponents try to guess their position.

  For more information on island shapes see the docs/screenshots folder.

  An island plays a role in three actions defined in the game: positioning
  islands, guessing coordinates, and checking for a forested island.

  Provided an island type and the starting coordinate the Island module can generate
  the full island.
  """

  @compile if Mix.env == :test, do: :export_all

  alias IslandsEngine.{Coordinate, Island, IslandTypes}

  @enforce_keys [:coordinates, :hit_coordinates]
  defstruct [:coordinates, :hit_coordinates]

  @island_types IslandTypes.types

  @doc """
  Construct an island based on its island type and its starting coordinate.
  """
  @spec new(atom, %Coordinate{}) :: {:ok, %Island{coordinates: MapSet.new([%Coordinate{}]), hit_coordinates: MapSet.new()}} | {:error, :invalid_island_type}
  def new(type, %Coordinate{} = upper_left) when type in @island_types do
  # def new(type, %Coordinate{} = upper_left) do
    # There are two conditions that have to be met in order to produce a valid island:
    # 1. the offsets/1 function has to return a list of offsets instead of an invalid island key error,
    # 2. add_coordinates/2 needs to return a MapSet instead of an invalid coordinate error.
    with [_|_] = offsets <- IslandTypes.offsets(type),
    %MapSet{} = coordinates <- add_coordinates(offsets, upper_left)
    do
      {:ok, %Island{coordinates: coordinates, hit_coordinates: MapSet.new()}}
    else
      # The else clause matches any error that might arise and passes it on. In practice,
      # we’re expecting this to be one of two types:
      # 1. {:error, :invalid_island_type} if we’ve provided the wrong type,
      # 2. {:error, :invalid_coordinate} if one of the offsets built an invalid coordinate.
      error -> error
    end
  end

  # @spec new(any, any) :: {:error, :invalid_island_type}
  def new(_type, _upper_left), do: {:error, :invalid_island_type}

  @doc """
  Check whether a new island overlaps with any existing islands.
  """
  @spec overlaps?(%Island{}, %Island{}) :: boolean
  def overlaps?(%Island{} = existing_island, %Island{} = new_island), do:
    # We’re representing islands as sets of coordinates.
    # Look for any coordinates that they have in common. If there are any common
    # coordinates between two islands, then those islands overlap.
    # FYI - Disjointed sets share no members.
    not MapSet.disjoint?(existing_island.coordinates, new_island.coordinates)

  @doc """
  Guess if a coordinate is a member of an island's coordinates.
  Track the correct guesses (hits) for the island.
  """
  @spec guess(%Island{}, %Coordinate{}) :: {:ok, %Island{hit_coordinates: %Coordinate{}}} | :miss
  def guess(%Island{} = island, %Coordinate{} = coordinate) do
    case MapSet.member?(island.coordinates, coordinate) do
      true ->
        hit_coordinates = MapSet.put(island.hit_coordinates, coordinate)
        {:hit, %{island | hit_coordinates: hit_coordinates}}
      false -> :miss
    end
  end

  @doc """
  Is the island forested (all island coordinates have been guessed)?
  """
  @spec forested?(%Island{}) :: boolean
  def forested?(%Island{} = island), do: MapSet.equal?(island.coordinates, island.hit_coordinates)

  # Construct an island based on a set of island type offsets and the starting coordinate.
  # Build the island insuring each coordinate is valid
  @spec add_coordinates([{integer, integer}, ...], {integer, integer}) :: {:cont, %MapSet{}} | {:halt, {:error, :invalid_coordinate}}
  defp add_coordinates(offsets, upper_left) do
    # Enum.reduce_while/3 takes an enumerable, a starting value for an accumulator,
    # and a function to apply to each enumerated value.
    # These three arguments will be the list of offsets, a new MapSet, and the function
    # add_coordinate/3.
    # The function add_coordinate/3 we pass to Enum.reduce_while/3 must return one of two tagged tuples:
    # either {:cont, some_value} to continue the enumeration, or {:halt, some_value} to end it.
    Enum.reduce_while(offsets, MapSet.new(), fn offset, acc ->
      add_coordinate(acc, upper_left, offset)
    end)
  end

  # Construct an island Coordinate based on the starting {row, column} and related offsets,
  # and add to the set of island Coordinates. Insure the island coordinate set does
  # not exceed the predefined range of the board.
  @spec add_coordinate(%MapSet{}, {integer, integer}, {integer, integer}) :: {:cont, %MapSet{}} | {:halt, {:error, :invalid_coordinate}}
  defp add_coordinate(coordinates, %Coordinate{row: row, col: col}, {row_offset, col_offset}) do
    case Coordinate.new(row + row_offset, col + col_offset) do
      {:ok, coordinate} ->
        {:cont, MapSet.put(coordinates, coordinate)}
      {:error, :invalid_coordinate} ->
        {:halt, {:error, :invalid_coordinate}}
    end
  end

end
