defmodule IslandsEngine.Guesses do
  @moduledoc """
  The set of island hits and misses coordinates for a players board.

  As players guess coordinates in the game, Guesses keep track of those
  coordinates so that their opponent’s board is accurately represented.
  Guessed coordinates are never removed, they are only added.

  The opponent’s board is nothing more than a group of guessed coordinates separated
  into those that hit an island and those that missed. There will be a large
  number of unguessed coordinates as well, but we can assume these are plain “ocean” coordinates.

  ## Examples

      iex> alias IslandsEngine.{Coordinate, Guesses}
      [IslandsEngine.Coordinate, IslandsEngine.Guesses]
      # create hit and miss coordinates
      iex> {:ok, hit_coordinate} = Coordinate.new 8, 3
      {:ok, %IslandsEngine.Coordinate{col: 3, row: 8}}
      iex> {:ok, miss_coordinate} = Coordinate.new 9, 7
      {:ok, %IslandsEngine.Coordinate{col: 7, row: 9}}
      # create the Guesses board and validate the hits and misses counts are zero
      iex> guesses = Guesses.new
      %IslandsEngine.Guesses{hits: %MapSet{}, misses: %MapSet{}}
      iex> MapSet.size guesses.hits
      0
      iex> MapSet.size guesses.misses
      0
      # add the hit and miss coordinates to the Guesses board
      iex> guesses = Guesses.add(guesses, :hit, hit_coordinate)
      iex> MapSet.size guesses.hits
      1
      iex> MapSet.size guesses.misses
      0
      iex> guesses = Guesses.add(guesses, :miss, miss_coordinate)
      iex> MapSet.size guesses.hits
      1
      iex> MapSet.size guesses.misses
      1
      # attempt adding an unknown type of guess to the board
      iex> Guesses.add(guesses, :unknown, miss_coordinate)
      {:error, :invalid_guess}
      iex> MapSet.size guesses.hits
      1
      iex> MapSet.size guesses.misses
      1
      # add the hit coordinate again and check the number of hits are unchanged
      iex> guesses = Guesses.add(guesses, :hit, hit_coordinate)
      Guesses.add(guesses, :hit, hit_coordinate)
      iex> MapSet.size guesses.hits
      1
      iex> MapSet.size guesses.misses
      1
  """

  alias IslandsEngine.{Coordinate, Guesses}

  @enforce_keys [:hits, :misses]
  defstruct [:hits, :misses]

  @doc """
  Create a new Guesses structure for tracking a player's hits and misses.
  """
  @spec new :: %Guesses{}
  def new, do: %Guesses{hits: MapSet.new, misses: MapSet.new}  # maintain set of unique guesses

  @doc """
  Add a coordinate as a :hit or a :miss to the collection of guesses.
  """
  @spec add(%Guesses{}, :hit | :miss, %Coordinate{}) :: %Guesses{hits: %Coordinate{}, misses: %Coordinate{}} | {:error, :invalid_guess}
  def add(%Guesses{} = guesses, :hit, %Coordinate{} = coordinate), do:
    # see: Kernel.update_in/2
    # update_in/2 takes a path to the nested data structure we want to update and a function to transform its value.
    update_in(guesses.hits, &MapSet.put(&1, coordinate))

  # @spec add(%Guesses{}, :miss, %Coordinate{}) :: %Guesses{hits: %Coordinate{}, misses: %Coordinate{}}
  def add(%Guesses{} = guesses, :miss, %Coordinate{} = coordinate), do:
    update_in(guesses.misses, &MapSet.put(&1, coordinate))

  # @spec add(any, any, any) :: {:error, :invalid_guess}
  def add(_guesses, _hit_or_miss, _coordinate), do: {:error, :invalid_guess}

end
