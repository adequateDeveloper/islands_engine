defmodule IslandsEngine.GameSupervisor do
  @moduledoc false

  use DynamicSupervisor

  alias IslandsEngine.{GameServer, StateManager}


  ## API

  # __MODULE__ evaluates to GameSupervisor, which is the local name of the supervisor process.
  # The supervisor Behaviour will translate this into the supervisor process PID.

  # Called at startup from Application.start/2
  def start_link(_arg), do: DynamicSupervisor.start_link(__MODULE__, [], name: __MODULE__)

  #
  # delegated top-level public interface client call from IslandsEngine
  #

  # start the new game and manage via this supervisor
  def new_game(player1_name) when is_binary(player1_name) do
    DynamicSupervisor.start_child(__MODULE__,
      %{id: GameServer, start: {GameServer, :start_link, [player1_name]}, restart: :transient})
  end

  def stop_game(player1_name) when is_binary(player1_name) do
    StateManager.delete_state(player1_name)
    DynamicSupervisor.terminate_child(__MODULE__, pid_from_player_name(player1_name))
  end


  ## Callbacks

  @impl true
  def init(_arg), do: DynamicSupervisor.init(strategy: :one_for_one)


  defp pid_from_player_name(player_name) do
    player_name
    |> GameServer.via_tuple()
    |> GenServer.whereis()
  end

end
