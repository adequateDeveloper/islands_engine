defmodule IslandsEngine.IslandTypes do
  @moduledoc """
  The defined set of island types and their type offsets for island construction.

  There are five different island types - :atoll, :dot, :l_shape, :s_shape, and
  :square.

  ## Examples

      # get the list of island types
      iex> alias IslandsEngine.IslandTypes
      IslandsEngine.IslandTypes
      iex> IslandTypes.types
      [:atoll, :dot, :l_shape, :s_shape, :square]
      # get the offsets for an island type
      iex> IslandTypes.offsets :square
      [{0, 0}, {0, 1}, {1, 0}, {1, 1}]
      iex> IslandTypes.offsets :bad_type
      {:error, :invalid_island_type}
  """

  @spec types :: [:atoll | :dot | :l_shape | :s_shape | :square, ...]
  def types, do: [:atoll, :dot, :l_shape, :s_shape, :square]

  # A series of offsets from a starting coordinate required to construct an island type
  @spec offsets(atom) :: [{integer, integer}, ...] | {:error, :invalid_island_type}
  def offsets(:dot),     do: [{0, 0}]
  def offsets(:l_shape), do: [{0, 0}, {1, 0}, {2, 0}, {2, 1}]
  def offsets(:square),  do: [{0, 0}, {0, 1}, {1, 0}, {1, 1}]
  def offsets(:s_shape), do: [{0, 1}, {0, 2}, {1, 0}, {1, 1}]
  def offsets(:atoll),   do: [{0, 0}, {0, 1}, {1, 1}, {2, 0}, {2, 1}]
  def offsets(_type),    do: {:error, :invalid_island_type}

end
