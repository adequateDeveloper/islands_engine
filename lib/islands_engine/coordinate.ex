defmodule IslandsEngine.Coordinate do
  @moduledoc """
  The basic unit of player boards and islands. Identified by the combination
  of numbers for row and column. A coordinate lies between a range of 1..10
  in both row and column.

  The Coordinate module will turn the row and column value pair into a coordinate.

  ## Examples

      iex> alias IslandsEngine.Coordinate
      IslandsEngine.Coordinate
      iex> Coordinate.new 1, 10
      {:ok, %IslandsEngine.Coordinate{col: 10, row: 1}}
      iex> Coordinate.new 1, 11
      {:error, :invalid_coordinate}
  """

  alias __MODULE__

  @enforce_keys [:row, :col]
  defstruct [:row, :col]

  # TODO: make this configurable?
  @board_range 1..10

  @doc """
  Create a new Coordinate struct.
  """
  @spec new(1..10, 1..10) :: {:ok, %Coordinate{}} | {:error, :invalid_coordinate}
  def new(row, col) when row in(@board_range) and col in(@board_range), do:
    {:ok, %Coordinate{row: row, col: col}}

  def new(_row, _col), do: {:error, :invalid_coordinate}

end
