defmodule IslandsEngine.Board do
  @moduledoc """
  A player’s own board acts as an interface. It’s the front door to the data and
  functions that make up the game.

  The player’s board contains their islands, and it also brokers messages for
  them. Sometimes it needs to reference individual islands, like when it’s
  checking to see if one is forested. Sometimes it needs to enumerate over
  all the islands, when it’s checking for a win, for instance.

  The board has a dual role to play. It knows about and can address all the
  islands. It can delegate function calls down to them individually or as a group.
  That makes a board both an orchestrator as well as an interface for actions
  that involve islands.

  The actions that a board needs to handle include positioning islands, ensuring
  that all islands are positioned, and guessing coordinates.

  In the course of the game, players can move their islands around as often as
  they want until they declare them set. After both players declare their islands
  set, it will be the first player’s turn to guess.

  Each time a player moves an island, the front end of the application will
  pass down an atom key representing the type of the island, as well as the row
  and column of the starting coordinate.

  The Board module knows how to position an island with a key and a full island.

  The Board insures that a player has positioned an island for all the island
  types before declaring their islands set.
  """

  @compile if Mix.env == :test, do: :export_all

  alias IslandsEngine.{Coordinate, Island, IslandTypes}

  @island_types IslandTypes.types

  @doc """
  Create a new game board.
  """
  @spec new :: map
  def new, do: %{}

  @doc """
  Position the island on the game board map with its key.
  First insure the island does not overlap existing islands.
  Players will be able to move their islands around the board until they declare
  them set.
  """
  @spec position_island(map, atom, %Island{}) :: %{atom: %Island{}} | {:error, :overlapping_island}
  def position_island(%{} = board, key, %Island{} = island) when key in @island_types do
    case overlaps_existing_island?(board, key, island) do
      true  -> {:error, :overlapping_island}
      false -> Map.put(board, key, island)
    end
  end

  @doc """
  Does the game board have an island positioned for all island types?
  """
  @spec all_islands_positioned?(map) :: boolean
  def all_islands_positioned?(%{} = board), do:
    Enum.all?(IslandTypes.types, &(Map.has_key?(board, &1)))

  @doc """
  Guess the coordinate of an island on the game board.
  The purpose of the guess/2 function is to reply with four pieces of information:
    1. whether the guess was a :hit or a :miss,
    2. either :none or the type of island that was forested,
    3. :win or :no_win,
    4. and finally the board map itself.
  """
  @spec guess(map, %Coordinate{}) :: {:hit | :mis, :none | atom, :win | :no_win, map}
  def guess(%{} = board, %Coordinate{} = coordinate) do
    board
    |> check_all_islands(coordinate)
    |> guess_response(board)
  end

  # Check all islands on the game board for a match of the guess coordinate.
  @spec check_all_islands(map, %Coordinate{}) :: {atom, %Island{}} | boolean
  defp check_all_islands(board, coordinate) do
    Enum.find_value(board, :miss, fn {key, island} ->
      case Island.guess(island, coordinate) do
        {:hit, island} -> {key, island}
        :miss          -> false
      end
    end)
  end

  # The guess was a hit.
  # Check whether the hit causes a forested island and also winning the game.
  @spec guess_response({atom, %Island{}}, map) :: {atom, atom, atom, map}
  defp guess_response({key, island}, board) do
    board = %{board | key => island}
    {:hit, forest_check(board, key), win_check(board), board}
  end

  # The guess was a miss.
  @spec guess_response(atom, map) :: {:miss, :none, :no_win, map}
  defp guess_response(:miss, board), do: {:miss, :none, :no_win, board}

  # Check whether the island is forested.
  @spec forest_check(map, atom) :: atom | :none
  defp forest_check(board, key) do
    case forested?(board, key) do
      true  -> key
      false -> :none
    end
  end

  #  Determine if all islands on the game board are now forested.
  @spec win_check(map) :: :win | :no_win
  defp win_check(board) do
    case all_forested?(board) do
      true  -> :win
      false -> :no_win
    end
  end

  # Determine if all islands on the board are forested.
  @spec all_forested?(map) :: boolean
  defp all_forested?(board), do:
    Enum.all?(board, fn {_key, island} -> Island.forested?(island) end)

  # Get the island from the game board and pass to Island.forested?/1 for evaluation.
  @spec forested?(map, atom) :: boolean
  defp forested?(board, key) do
    board
    |> Map.fetch!(key)
    |> Island.forested?()
  end

  # Does the new island overlap any existing islands on the board?
  # Ignores new_island with new_key == existing key
  @spec overlaps_existing_island?(%{}, atom, %Island{}) :: boolean
  defp overlaps_existing_island?(board, new_key, new_island) do
    Enum.any?(board, fn {key, island} ->
      if key != new_key do
        Island.overlaps?(island, new_island)
      end
    end)
  end

end
