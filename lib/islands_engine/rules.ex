defmodule IslandsEngine.Rules do
  @moduledoc """
  A Finite State Machine to manage state transitions for Islands Engine.

  It makes decisions for the application about which actions to allow and
  which to deny at a given state. It manages transitions from one state
  to the next, and enforces the game rules.

  Implementing the state machine keeps the logic for managing state separate
  from the rest of the game logic. This separation of concerns keeps the code
  clean, readable, and maintainable.

  Each time the check/2 function is invoked the current state as well as an event are passed in.
  The function will decide whether to permit the combination of state and event.
  It will also decide whether to transition to a new state or not.
  If a tuple tagged with the :ok atom is returned, then the combination is permissible.
  If the atom :error is returned, then the combination is not permissible.

  ## States and their Transitions:

  The following is a representation of the state machine with all the states and the direction
  of the transitions between them:

      initialized --> players_set --> player1_turn <--> player2_turn --> game_over
                                           |_________________|_______________^

  ## States and their Events:

  The following is a representation of each of the state machine states
  and the events messages accepted while in that state:

      Current State     Accepted Event
      -------------     --------------
      initialized       :add_player
      players_set       {:position_island, playerN}
                        {:set_island, playerN}
      player1_turn      {:guess_coordinate, :player1}
                        {:win_check, :no_win}
                        {:win_check, :win}
      player2_turn      {:guess_coordinate, :player2}
                        {:win_check, :no_win}
                        {:win_check, :win}
      game_over         ----------


  ## Examples
      iex> alias IslandsEngine.Rules
      IslandsEngine.Rules
      # Initialized with player islands not set:
      iex> rules = Rules.new
      %IslandsEngine.Rules{
        player1: :islands_not_set,
        player2: :islands_not_set,
        state: :initialized
      }
      # Transition to :players_set state via event :add_player:
      iex> {:ok, rules} = Rules.check rules, :add_player
      {:ok,
      %IslandsEngine.Rules{
        player1: :islands_not_set,
        player2: :islands_not_set,
        state: :players_set
      }}
      # Players allowed to position islands without changing state:
      iex> {:ok, rules} = Rules.check rules, {:position_islands, :player1}
      {:ok,
       %IslandsEngine.Rules{
         player1: :islands_not_set,
         player2: :islands_not_set,
         state: :players_set
       }}
      iex> {:ok, rules} = Rules.check rules, {:position_islands, :player2}
      {:ok,
       %IslandsEngine.Rules{
         player1: :islands_not_set,
         player2: :islands_not_set,
         state: :players_set
       }}
      #  PlayerN "locks" island locations via event :set_islands:
      iex> {:ok, rules} = Rules.check rules, {:set_islands, :player1}
      {:ok,
       %IslandsEngine.Rules{
         player1: :islands_set,
         player2: :islands_not_set,
         state: :players_set
       }}
      # PlayerN cannot reposition islands after they have been set:
      iex> Rules.check rules, {:position_islands, :player1}
      :error
      # The opposite player can continue to position islands and eventually set islands:
      iex> {:ok, rules} = Rules.check rules, {:position_islands, :player2}
      {:ok,
       %IslandsEngine.Rules{
         player1: :islands_set,
         player2: :islands_not_set,
         state: :players_set
       }}
      iex> {:ok, rules} = Rules.check rules, {:set_islands, :player2}
      {:ok,
       %IslandsEngine.Rules{
         player1: :islands_set,
         player2: :islands_set,
         state: :player1_turn
       }}
      # Once both sets of islands are set, players may only take turns guessing coordinates:
      iex> Rules.check rules, {:position_islands, :player2}
      :error
      iex> Rules.check rules, {:set_islands, :player2}
      :error
      iex> Rules.check rules, :add_player
      :error
      iex> Rules.check rules, {:position_islands, :player1}
      :error
      iex> Rules.check rules, {:position_islands, :player2}
      :error
      iex> Rules.check rules, {:set_islands, :player1}
      :error
      iex> Rules.check rules, {:guess_coordinate, :player2}
      :error
      iex> {:ok, rules} = Rules.check rules, {:guess_coordinate, :player1}
      {:ok,
       %IslandsEngine.Rules{
         player1: :islands_set,
         player2: :islands_set,
         state: :player2_turn
       }}
      iex> {:ok, rules} = Rules.check rules, {:guess_coordinate, :player2}
      {:ok,
       %IslandsEngine.Rules{
         player1: :islands_set,
         player2: :islands_set,
         state: :player1_turn
       }}
      # Either player may check if the game is won when it's their turn:
      iex> {:ok, rules} = Rules.check rules, {:win_check, :no_win}
      {:ok,
       %IslandsEngine.Rules{
         player1: :islands_set,
         player2: :islands_set,
         state: :player1_turn
       }}
      iex> {:ok, rules} = Rules.check rules, {:win_check, :win}
      {:ok,
       %IslandsEngine.Rules{
         player1: :islands_set,
         player2: :islands_set,
         state: :game_over
       }}
      # No events are accepted once the game is over:
      iex> Rules.check rules, {:win_check, :win}
      :error
  """

  @compile if Mix.env == :test, do: :export_all

  alias __MODULE__

  defstruct [
    state: :initialized,
    player1: :islands_not_set,
    player2: :islands_not_set,
  ]

  # @players [:player1, :player2]

  @doc """
  Create a new Rules structure for system state.
  """
  @spec new :: %Rules{state: :initialized, player1: :islands_not_set, player2: :islands_not_set}
  def new, do: %Rules{}

  @doc """
  Makes a decision about whether it's ok to:
    1. add another player based on the current state of the game
    2. position and set islands on the game board
    3. guess coordinates of opposing player islands
    4. determine whether a game has been won for the current player

  Returns {:ok, updated_rules_state} for each call.
  For any state/event combination that ends up in error an :error atom is returned,
  and the transition of state is left unchanged.
  """

  # States and their Transitions:

  # From State    Event                         To State      player1 value     player2 value
  # -----------------------------------------------------------------------------------------
  # :initialized  {:add_player}                 :players_set  :islands_not_set  :islands_not_set

  # :players_set  {:position_islands, player#}  :players_set  :islands_not_set  :islands_not_set

  # :players_set  {:set_islands, player1}       :players_set  :islands_set      :islands_not_set

  # :players_set  {:set_islands, player2}       :player1_turn :islands_set      :islands_set

  # Notes:
  # In :players_set state, either player can position and reposition their islands on the board
  # without transitioning the state. Players can move their islands at any time until they set them.

  # If player1 has set their islands but player2 hasn’t, player1 should no longer be able to move
  # their islands, but player2 should still be able to. While this condition exists, the state machine
  # will remain in the :players_set state.

  # The second player setting their islands is the event that triggers the state change from
  # :players_set to :player1_turn.


  # From State     Event                          To State       player1 value  player2 value
  # -----------------------------------------------------------------------------------------
  # :player1_turn  {:guess_coordinate, :player1}  :player2_turn  :islands_set   :islands_set

  # :player2_turn  {:guess_coordinate, :player2}  :player1_turn  :islands_set   :islands_set

  # :player1_turn  {:win_check, :no_win}          :player1_turn  :islands_set   :islands_set

  # :player2_turn  {:win_check, :no_win}          :player2_turn  :islands_set   :islands_set

  # :player1_turn  {:win_check, :win}             :game_over     :islands_set   :islands_set

  # :player2_turn  {:win_check, :win}             :game_over     :islands_set   :islands_set

  # Notes:
  # We’re at the point where both players have set their islands, and the game state is in
  # :player1_turn . When it’s the first player’s turn, that player may guess a coordinate,
  # and that player may win the game. No other events are permissible.

  # When :player1 guesses a coordinate, the state will transition to :player2_turn, and vice versa.
  # And when one of the players wins, the state should transition to :game_over
  # -----------------------------------------------------------------------------------------


  # When in :initialized state, the only event accepted is :add_player which transitions state to :players_set.
  @spec check(%Rules{}, atom) :: {:ok, %Rules{}} | :error
  def check(%Rules{state: :initialized} = rules, :add_player = _event), do:
    {:ok, %Rules{rules | state: :players_set}}

  # When in :players_set state, its ok for either of the players to position their islands.
  # Only allowed to position islands when *player state* is :islands_not_set.
  # Neither action is enough to transition the rules state out of :players_set
  def check(%Rules{state: :players_set} = rules, {:position_islands, player} = _event) do
    case Map.fetch! rules, player do
      :islands_set -> :error
      :islands_not_set -> {:ok, rules}
    end
  end

  # When in :players_set state with :set_islands event then transition to state :player1_turn
  # only when both players state set to :islands_set
  def check(%Rules{state: :players_set} = rules, {:set_islands, player} = _event) do
    new_rules = Map.put rules, player, :islands_set

    case both_players_islands_set?(new_rules) do
      true -> {:ok, %Rules{new_rules | state: :player1_turn}}
      false -> {:ok, new_rules}
    end
  end

  # When in :player1_turn state with :guess_coordinate event then transition to state :player2_turn
  def check(%Rules{state: :player1_turn} = rules, {:guess_coordinate, :player1} = _event), do:
    {:ok, %Rules{rules | state: :player2_turn}}

  # When in :player1_turn state with {:win_check, :win} event then transition to state :game_over
  def check(%Rules{state: :player1_turn} = rules, {:win_check, :win} = _event), do:
    {:ok, %Rules{rules | state: :game_over}}

  # When in :player1_turn state with {:win_check, :no_win} event then return existing state
  def check(%Rules{state: :player1_turn} = rules, {:win_check, :no_win} = _event), do:
    {:ok, rules}

  # When in :player2_turn state with :guess_coordinate event then transition to state :player1_turn
  def check(%Rules{state: :player2_turn} = rules, {:guess_coordinate, :player2} = _event), do:
    {:ok, %Rules{rules | state: :player1_turn}}

  # When in :player2_turn state with {:win_check, :win} event then transition to state :game_over
  def check(%Rules{state: :player2_turn} = rules, {:win_check, :win} = _event), do:
    {:ok, %Rules{rules | state: :game_over}}

  # When in :player2_turn state with {:win_check, :no_win} event then return existing state
  def check(%Rules{state: :player2_turn} = rules, {:win_check, :no_win} = _event), do:
    {:ok, rules}

  # None of the previous conditions have been met. Return an error and leave state unchanged.
  def check(_state, _event), do: :error

  # have both players set their islands?
  @spec both_players_islands_set?(%Rules{}) :: boolean
  defp both_players_islands_set?(rules), do:
    rules.player1 == :islands_set && rules.player2 == :islands_set

end
