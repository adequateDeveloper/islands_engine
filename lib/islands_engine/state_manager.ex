defmodule IslandsEngine.StateManager do
  @moduledoc false

  # Storing and Retrieving Game State from ETS

  # Store the full state for each game in the :game_state table whenever it changes.
  # Each game process is registered by the name of the first player, so that name
  # is the key to store the state under.

  # For game processes, some state will change whenever there is a successful
  # reply — either in the game itself or in the state machine data.
  # The single function Game.reply_success/2 handles all successful replies.


  @table_name :game_state

  # called by Application
  ## table type: set (default)
  # def init(), do: :ets.new(@table_name, [:public, :named_table])
  def init(), do: PersistentEts.new(@table_name, "#{Atom.to_string(@table_name)}.tab", [:public, :named_table])

  # key is expected to be the name of :player1
  # value is expected to be the full game state
  def store_state(key, value) when is_binary(key) and is_map(value) do
    :ets.insert(@table_name, {key, value})
  end

  # key is expected to be the name of :player1
  def get_state(key) when is_binary(key) do
    :ets.lookup(@table_name, key)
  end

  def delete_state(key) when is_binary(key) do
    :ets.delete(@table_name, key)
  end

end
