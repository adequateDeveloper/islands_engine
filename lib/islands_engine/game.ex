defmodule IslandsEngine.Game do
  @moduledoc false

# The Game module is the 'game controller' of the application. It has two responsibilities:
#   - orchestrate the logic that exists in all the game-related modules.
#   - package the new game state for return to the OTP callbacks (see docs/GenServer_CheatSheet.pdf).
#
# Game-related Modules:
# - The Board module knows how to position an island with a key and a full island.
# - The Coordinate module can turn the row and column value into a coordinate,
# - and the Island module can turn the island key and the coordinate into a full island.
#
# The Game module will coordinate between these, check to see that all the values are valid, and formulate a response.

  alias IslandsEngine.{Board, Coordinate, Guesses, Island, Rules, StateManager}

  @compile if Mix.env == :test, do: :export_all

  # a day’s worth of milliseconds
  @timeout 60 * 60 * 24 * 1000

  # game processes will time themselves out when they’re inactive for 15 seconds.
  # @timeout 15000

  def timeout(), do: @timeout



  # Islands requires two players for each game. Each player needs a name, a board, and guesses representing their
  # opponent’s board. The game itself need a rules struct so it can manage the game.
  def init(player1_name) do
    player1 = %{name: player1_name, board: Board.new(), guesses: Guesses.new()}
    player2 = %{name: nil, board: Board.new(), guesses: Guesses.new()}
    {:ok, %{player1: player1, player2: player2, rules: %Rules{}}, @timeout}
  end

  def add_player(player2_name, state_of_game) do
    # do the rules allow adding another player?
    with {:ok, rules} <- Rules.check(state_of_game.rules, :add_player)
      do
      state_of_game
      |> update_player2_name(player2_name)
      |> update_rules(rules)
      |> reply_success(:ok)
    else
      :error -> state_of_game |> reply_error(:error)
    end
  end

  # Generate and position an island on the game board of the player
  # Check for the following preconditions: (the order is deliberate)
  #   the rules permit the player to position their islands
  #   the row and column values generate a valid coordinate
  #   the island type and the upper left coordinate (row, column) generate a valid island
  #   that positioning the island doesn’t generate an error
  def position_island(player, island_type, start_row, start_column, state_of_game) do

    player_board = player_board(state_of_game, player)

    with {:ok, new_rules}      <- Rules.check(state_of_game.rules, {:position_islands, player}),
         {:ok, new_coordinate} <- Coordinate.new(start_row, start_column),
         {:ok, new_island}     <- Island.new(island_type, new_coordinate),
         %{} = new_board       <- Board.position_island(player_board, island_type, new_island)
      do
      state_of_game
      |> update_board(player, new_board)
      |> update_rules(new_rules)
      |> reply_success(:ok)
    else
      :error -> state_of_game |> reply_error(:error)
      {:error, :invalid_coordinate}  -> state_of_game |> reply_error({:error, :invalid_coordinate})
      {:error, :invalid_island_type} -> state_of_game |> reply_error({:error, :invalid_island_type})
    end
  end

  # Mark the player's board as set
  # Check for the following preconditions:
  #   the rules permit the player to set their islands
  #   does the player's game board have an island positioned for all island types?
  def set_islands(player, state_of_game) do

    player_board = player_board(state_of_game, player)

    with {:ok, new_rules} <- Rules.check(state_of_game.rules, {:set_islands, player}),
         true             <- Board.all_islands_positioned?(player_board)
      do
      state_of_game
      |> update_rules(new_rules)
      |> reply_success(:ok)
    else
      :error -> state_of_game |> reply_error(:error)
      false  -> state_of_game |> reply_error({:error, :not_all_islands_positioned})
    end
  end

  # Guess an island coordinate on the opponent’s board
  # Check for the following preconditions:
  #   the rules allow the given player to guess
  #   the row and column values make a valid coordinate
  #   the guess was:
  #     a hit or a miss
  #     whether it forested and island
  #     whether it won the game
  def guess_coordinate(player, start_row, start_column, state_of_game) do
    opponent = opponent(player)
    opponent_board = player_board(state_of_game, opponent)

    with {:ok, new_rules}      <- Rules.check(state_of_game.rules, {:guess_coordinate, player}),
         {:ok, new_coordinate} <- Coordinate.new(start_row, start_column),
         {hit_or_miss, forested_island, win_status, opponent_board}
                               <- Board.guess(opponent_board, new_coordinate),
         {:ok, new_rules}      <- Rules.check(new_rules, {:win_check, win_status})
      do
      state_of_game
      |> update_board(opponent, opponent_board)
      |> update_guesses(player, hit_or_miss, new_coordinate)
      |> update_rules(new_rules)
      |> reply_success({hit_or_miss, forested_island, win_status})
    else
      # :error -> {:reply, :error, state_of_game}
      # {:error, :invalid_coordinate} -> {:reply, {:error, :invalid_coordinate}, state_of_game}

      :error -> state_of_game |> reply_error(:error)
      {:error, :invalid_coordinate} -> state_of_game |> reply_error({:error, :invalid_coordinate})
    end
  end


  # note: Kernal.put_in/2 transform values nested in a map and returns the whole, transformed map.

  defp update_player2_name(state, player2_name), do: put_in(state.player2.name, player2_name)

  defp update_rules(state, rules), do: %{state | rules: rules}

  # All successfulreplies — either in the game itself or in the state machine data are handled here.
  defp reply_success(state, reply) do
    StateManager.store_state(state.player1.name, state)
    {:reply, reply, state, @timeout}
  end

  defp reply_error(state, reply), do: {:reply, reply, state, @timeout}

  # extract player's board from the game state
  defp player_board(state, player), do: Map.get(state, player).board

  defp update_board(state, player, board), do: Map.update! state, player, fn player -> %{player | board: board} end

  defp opponent(:player1), do: :player2

  defp opponent(:player2), do: :player1

  # note: Kernal.update_in/2 allows us to easily update nested data.

  defp update_guesses(state_data, player_key, hit_or_miss, coordinate) do
    update_in(state_data[player_key].guesses, fn guesses ->
      Guesses.add(guesses, hit_or_miss, coordinate)
    end)
  end

end
