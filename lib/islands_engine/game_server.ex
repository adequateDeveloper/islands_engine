defmodule IslandsEngine.GameServer do
  @moduledoc false

  # The 'use GenServer' triggers a macro that compiles default implementations
  # for all the GenServer callbacks into the Server module.
  use GenServer

  alias IslandsEngine.{Game, IslandTypes, StateManager}

  @players [:player1, :player2]

  @island_types IslandTypes.types

  #
  # delegated top-level public interface client calls from IslandsEngine
  #

    # Called from GameSupervisor
    def start_link(player1_name) when is_binary(player1_name) do
      GenServer.start_link(__MODULE__, player1_name, name: via_tuple(player1_name))
    end


  # NOTE: Moved to GameSupervisor
  # @spec new_game(binary()) :: pid()
  # def new_game(player1_name) when is_binary(player1_name) do
  #   # specify which supervisor, and the child spec of the process to be supervised
  #   {:ok, pid} = DynamicSupervisor.start_child IslandsEngine.Supervisor, {__MODULE__, [player1_name]}
  #   pid
  # end

  @spec add_player(pid(), binary()) :: any()
  def add_player(game_pid, player2_name) when is_pid(game_pid) and is_binary(player2_name), do:
    GenServer.call(game_pid, {:add_player, player2_name})

  # NOTE: to allow an invalid island type to be passed along rather than have a function clause error generated here,
  #       remove the function clause ' and island_type in @island_types' below.
  #       The error returned will be '{:error, :invalid_island_type}'

  @spec position_island(pid(), any(), any(), integer(), integer()) :: any()
  def position_island(game_pid, player, island_type, start_row, start_column)
    when is_pid(game_pid) and player in @players and island_type in @island_types
    and is_integer(start_row) and is_integer(start_column), do:
      GenServer.call(game_pid, {:position_island, player, island_type, start_row, start_column})

  @spec set_islands(pid(), :player1 | :player2) :: any()
  def set_islands(game_pid, player) when is_pid(game_pid) and player in @players, do:
    GenServer.call(game_pid, {:set_islands, player})

  @spec guess_coordinate(pid(), any(), integer(), integer()) :: any()
  def guess_coordinate(game_pid, player, start_row, start_column)
    when is_pid(game_pid) and player in @players and is_integer(start_row) and is_integer(start_column), do:
      GenServer.call(game_pid, {:guess_coordinate, player, start_row, start_column})

  @spec via_tuple(any) :: {:via, Registry, {Registry.Game, any}}
  def via_tuple(game_name), do: {:via, Registry, {Registry.Game, game_name}}


  #
  # GenServer callbacks
  #

  # init/1 blocks while it is evaluating.
  # If the StateManager lookup happened here and took a long time, this would block
  # the IslandsEngine.new_game/1 function until init/1 returned.
  # In a really busy system, that could cause problems.
  # We fix this by having the process send a message to itself inside init/1 asking to set the state.
  # This lets init/1 return immediately, and the {:set_state, player1_name} message will appear in the new process’s mailbox.

  # Any time a process is either started or restarted, GenServer will trigger the init/1 callback.
  # Insure that either the current or new game state is stored.
  @impl true
  def init(player1_name) do
    send(self(), {:set_state, player1_name})

    # fresh_state/1 is called here again after it is potentially called in the handle_info/2 :set_state.
    # This way the freshest state is returned to the game.
    {:ok, fresh_state(player1_name)}
  end

  @impl true
  def handle_call({:add_player, player2_name}, _from, state) do
    Game.add_player player2_name, state
  end

  @impl true
  def handle_call({:position_island, player, island_type, start_row, start_column}, _from, state) do
    Game.position_island player, island_type, start_row, start_column, state
  end

  @impl true
  def handle_call({:set_islands, player}, _from, state) do
    Game.set_islands player, state
  end

  @impl true
  def handle_call({:guess_coordinate, player, start_row, start_column}, _from, state) do
    Game.guess_coordinate player, start_row, start_column, state
  end

  # called by init/1
  # Check for existing game state and re-store. If not found then store a new game state.
  @impl true
  def handle_info({:set_state, player1_name}, _state) do
    new_state =
      case StateManager.get_state(player1_name) do
        [] -> fresh_state(player1_name)
        [{_key, current_state}] -> current_state
      end

    StateManager.store_state(player1_name, new_state)

    # Game.timout/0 is set here because handle_info/2 :set_state is the last callback
    # to return from the initial call to return from init/1.
    # There won't be a new message in the game process's mailbox after this,
    # so the timeout will be able to do its job.
    {:noreply, new_state, Game.timeout()}
  end

  # Tagging the return tuple with :stop causes the GenServer Behaviour to trigger the default terminate/2 callback.
  # It will pass the middle term of the return tuple in as the first argument so we can pattern match on it.
  @impl true
  def handle_info(:timeout, state) do
    {:stop, {:shutdown, :timeout}, state}
  end

  @impl true
  def handle_info(message, state) do
    IO.puts("message '#{message}' not understood")
    {:noreply, state}
  end

  @impl true
  def terminate({:shutdown, :timeout}, state) do
    StateManager.delete_state(state.player1.name)
    :ok
  end

  # catchall clause
  def terminate(_reason, _state), do: :ok

  # Initialize a new game.
  # Called by handle_info/2 :set_state
  defp fresh_state(player1_name) do
    {:ok, new_state, _timeout} = Game.init player1_name
    new_state
  end


end
