defmodule IslandsEngine.Application do
  @moduledoc false

  use Application

  alias IslandsEngine.StateManager

  # See also: genserver_approach.txt

  # Start the Islands Engine component with a DynamicSupervisor as the root
  # supervisor and no worker processes (which are the individual games).

  # Because of the DynamicSupervisor each new game is dynamically added
  # to the supervisor' list only after the new game is successfully started.

  # Design Approach:
  # After Islands Engine component is started, new game processes are started
  # with a call to the public API lib/IslandsEngine.new_game/1.
  # This call, like all subsequent game calls in lib/IslandsEngine are delegated
  # to equivalent calls in the Islands Engine GenServer lib/islands_engine/Server.
  # During the course of the game, the Server then makes calls to the Islands Engine
  # 'game controller' lib/islands_engine/Game.

  def start(_type, _args) do

    # SEE: https://www.jkmrto.dev/posts/dynamic-supervisor-in-elixir/

    # for saving and restoring game state in case of a game process crash
    StateManager.init

    children = [
      # Starts a worker by calling: IslandsEngine.Worker.start_link(arg)
      # {ExampleRegistry.Worker, arg}

      # managing multiple 2-player games via game-names
      {Registry, keys: :unique, name: Registry.Game},

      # supervising unknown number of games via a Dynamic Supervisor
      {IslandsEngine.GameSupervisor, []}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    options = [
      name: IslandsEngine.Supervisor,
      strategy: :one_for_one,
    ]

    Supervisor.start_link(children, options)
  end

end
