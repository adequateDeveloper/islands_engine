defmodule IslandsEngine do
  @moduledoc """
  The public API to the Islands Engine component.
  """

  # NOTE: See islands_engine_examples.txt for usage exampes

  # Delegates public API implementations to GameSupervisor, GameServer
  alias IslandsEngine.{GameSupervisor, GameServer}

  @doc """
  Initialize a new game with a player name.
  """
  @spec new_game(binary) :: {:ok, pid}
  defdelegate new_game(player1_name), to: GameSupervisor

  @doc """
  Add the second player name to the game.
  """
  @spec add_player(pid, binary) :: atom
  defdelegate add_player(game_pid, player2_name), to: GameServer

  @doc """
  Position an island type on the player game board by providing its upper left starting coordinate.
  """
  @spec position_island(pid, atom, atom, integer, integer) :: atom
  defdelegate position_island(game_pid, player, island_type, start_row, start_column), to: GameServer

  @doc """
  Declare the islands as set on the player game board.
  """
  @spec set_islands(pid, atom) :: atom
  defdelegate set_islands(game_pid, player), to: GameServer

  @doc """
  Guess an island coordinate of the opposing player game board.
  """
  @spec guess_coordinate(pid, atom, integer, integer) :: atom
  defdelegate guess_coordinate(game_pid, player, start_row, start_column), to: GameServer

  @spec stop_game(binary) :: :ok | {:error, :not_found}
  defdelegate stop_game(player1_name), to: GameSupervisor

end
