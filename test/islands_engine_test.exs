defmodule IslandsEngineTest do
  use ExUnit.Case, async: true

#   doctest IslandsEngine

  setup do
    {:ok, new_game_pid} = IslandsEngine.new_game new_player_name
    %{new_game_pid: new_game_pid}
  end

  defp new_player_name, do: "test_player_#{:rand.uniform(1000)}"


  test "new_game/1 returns pid and sets initial game state", %{new_game_pid: game_pid} do
    assert is_pid game_pid

    state = :sys.get_state game_pid

    assert state.rules.state == :initialized
    assert state.rules.player1 == :islands_not_set
    assert state.rules.player2 == :islands_not_set

    refute state.player1.name == nil
    assert state.player2.name == nil

    assert state.player1.board == %{}
    assert state.player2.board == %{}

    assert MapSet.size(state.player1.guesses.hits) == 0
    assert MapSet.size(state.player1.guesses.misses) == 0

    assert MapSet.size(state.player2.guesses.hits) == 0
    assert MapSet.size(state.player2.guesses.misses) == 0
  end

  test "add_player/2 transitions game state", %{new_game_pid: game_pid} do
    state = :sys.get_state game_pid

    assert state.rules.state == :initialized

    test_player_name = "test_player_2"

    assert :ok == IslandsEngine.add_player game_pid, test_player_name

    state = :sys.get_state game_pid

    assert state.rules.state == :players_set
    assert state.player2.name == test_player_name
  end

  test "position_island/5 does not transition game state", %{new_game_pid: game_pid} do
    test_player_name = "test_player_2"
    IslandsEngine.add_player game_pid, test_player_name

    state = :sys.get_state game_pid
    assert state.rules.state == :players_set

    assert :ok == IslandsEngine.position_island(game_pid, :player1, :dot, 1, 1)
    assert :ok == IslandsEngine.position_island(game_pid, :player2, :dot, 1, 1)

    state = :sys.get_state game_pid
    assert state.rules.state == :players_set
  end

  test "position_island/5 requires both players first be set", %{new_game_pid: game_pid} do
    state = :sys.get_state game_pid
    assert state.rules.state == :initialized

    assert :error == IslandsEngine.position_island(game_pid, :player1, :dot, 1, 1)

    test_player_name = "test_player_2"
    IslandsEngine.add_player game_pid, test_player_name

    state = :sys.get_state game_pid
    assert state.rules.state == :players_set

    assert :ok == IslandsEngine.position_island(game_pid, :player1, :dot, 1, 1)
  end

  test "set_islands/2 updates player status but not game status", %{new_game_pid: game_pid} do
    IslandsEngine.add_player game_pid, "player2"
    state = :sys.get_state game_pid

    assert state.rules.state == :players_set
    assert state.rules.player1 == :islands_not_set
    assert state.rules.player2 == :islands_not_set

    IslandsEngine.position_island game_pid, :player1, :atoll, 1, 1
    IslandsEngine.position_island game_pid, :player1, :dot, 1, 4
    IslandsEngine.position_island game_pid, :player1, :l_shape, 1, 5
    IslandsEngine.position_island game_pid, :player1, :s_shape, 5, 1
    IslandsEngine.position_island game_pid, :player1, :square, 5, 5
    IslandsEngine.set_islands game_pid, :player1
    state = :sys.get_state game_pid

    assert state.rules.state == :players_set
    assert state.rules.player1 == :islands_set
    assert state.rules.player2 == :islands_not_set
  end

  test "set_islands/2 before all islands are set produces error", %{new_game_pid: game_pid} do
    IslandsEngine.add_player game_pid, "player2"

    assert {:error, :not_all_islands_positioned} == IslandsEngine.set_islands game_pid, :player1
  end

  # TODO: complete the test
  test "guess_coordinate/4 produces game win", %{new_game_pid: game_pid} do
    IslandsEngine.add_player game_pid, "player2"

    IslandsEngine.position_island game_pid, :player1, :dot, 1, 1
    IslandsEngine.position_island game_pid, :player1, :square, 1, 2
    IslandsEngine.position_island game_pid, :player1, :atoll, 1, 4
    IslandsEngine.position_island game_pid, :player1, :l_shape, 1, 6
    IslandsEngine.position_island game_pid, :player1, :s_shape, 1, 8
    IslandsEngine.set_islands game_pid, :player1

    IslandsEngine.position_island game_pid, :player2, :dot, 1, 1
    IslandsEngine.position_island game_pid, :player2, :square, 1, 2
    IslandsEngine.position_island game_pid, :player2, :atoll, 1, 4
    IslandsEngine.position_island game_pid, :player2, :l_shape, 1, 6
    IslandsEngine.position_island game_pid, :player2, :s_shape, 1, 8
    IslandsEngine.set_islands game_pid, :player2

    state = :sys.get_state(game_pid)
    state = :sys.replace_state(game_pid, fn _data -> %{state | rules: %IslandsEngine.Rules{state: :player1_turn}} end)
    assert :player1_turn == state.rules.state

    assert {:miss, :none, :no_win} == IslandsEngine.guess_coordinate game_pid, :player1, 10, 10
    assert :error == IslandsEngine.guess_coordinate game_pid, :player1, 9, 9
    assert {:hit, :dot, :no_win} == IslandsEngine.guess_coordinate game_pid, :player2, 1, 1
  end

  test "guess_coordinate/4 when state is :initialized produces :errror", %{new_game_pid: game_pid} do
    assert :error == IslandsEngine.guess_coordinate game_pid, :player1, 1, 1
  end

end
