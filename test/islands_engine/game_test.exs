defmodule IslandsEngine.GameTest do
  use ExUnit.Case, async: true
  alias IslandsEngine.Game

  # doctest Game

  test "init/1 constructs a new game" do
    {:ok, %{player1: player1, player2: player2, rules: rules}, _timeout} = Game.init "player1"

    assert "player1" == player1.name
    assert %{} == player1.board
    assert 0 == MapSet.size player1.guesses.hits
    assert 0 == MapSet.size player1.guesses.misses


    assert nil == player2.name
    assert %{} == player2.board
    assert 0 == MapSet.size player2.guesses.hits
    assert 0 == MapSet.size player2.guesses.misses

    assert :initialized == rules.state
    assert :islands_not_set == rules.player1
    assert :islands_not_set == rules.player2
  end

  test "add_player/2 adds the second player" do
    {:ok, state_of_game, _timeout} = Game.init "player1"

    {:reply, reply, state_of_game, _timeout} = Game.add_player "player2", state_of_game

    assert :ok == reply
    assert "player2" == state_of_game.player2.name
    assert :players_set == state_of_game.rules.state
  end

  test "add_player/2 attempts adding a third player" do
    {:ok, state_of_game, _timeout} = Game.init "player1"

    {:reply, _reply, state_of_game, _timeout} = Game.add_player "player2", state_of_game
    {:reply, reply, state_of_game, _timeout} = Game.add_player "player3", state_of_game

    assert :error == reply
    assert "player1" == state_of_game.player1.name
    assert "player2" == state_of_game.player2.name
    assert :players_set == state_of_game.rules.state
  end

  test "position_island/5 adds eligible island" do
    {:ok, state_of_game, _timeout} = Game.init "player1"
    {:reply, _reply, state_of_game, _timeout} = Game.add_player "player2", state_of_game

    {:reply, reply, _state_of_game, _timeout} = Game.position_island :player1, :dot, 1, 1, state_of_game

    assert :ok == reply
  end

  test "position_island/5 prevents use of invalid coordinate" do
    {:ok, state_of_game, _timeout} = Game.init "player1"
    {:reply, _reply, state_of_game, _timeout} = Game.add_player "player2", state_of_game

    {:reply, {:error, :invalid_coordinate}, _state_of_game, _timeout} = Game.position_island :player1, :square, 11, 1, state_of_game
  end

  test "position_island/5 prevents use of invalid island type" do
    {:ok, state_of_game, _timeout} = Game.init "player1"
    {:reply, _reply, state_of_game, _timeout} = Game.add_player "player2", state_of_game

    {:reply, {:error, :invalid_island_type}, _state_of_game, _timeout} = Game.position_island :player1, :sunknown, 1, 1, state_of_game
  end

  test "set_islands/2 marks player's board as set" do
    {:ok, state_of_game, _timeout} = Game.init "player1"
    {:reply, _reply, state_of_game, _timeout} = Game.add_player "player2", state_of_game
    {:reply, _reply, state_of_game, _timeout} = Game.position_island :player1, :dot, 1, 1, state_of_game
    {:reply, _reply, state_of_game, _timeout} = Game.position_island :player1, :square, 1, 2, state_of_game
    {:reply, _reply, state_of_game, _timeout} = Game.position_island :player1, :atoll, 1, 4, state_of_game
    {:reply, _reply, state_of_game, _timeout} = Game.position_island :player1, :l_shape, 1, 6, state_of_game
    {:reply, _reply, state_of_game, _timeout} = Game.position_island :player1, :s_shape, 1, 8, state_of_game

    {:reply, reply, _state_of_game, _timeout} = Game.set_islands :player1, state_of_game
    assert :ok == reply
  end

  test "set_islands/2 prevents marking player's board as set when all islands have not been set" do
    {:ok, state_of_game, _timeout} = Game.init "player1"
    {:reply, _reply, state_of_game, _timeout} = Game.add_player "player2", state_of_game

    assert {:reply, {:error, :not_all_islands_positioned}, state_of_game, _timeout} == Game.set_islands :player1, state_of_game
  end
end
