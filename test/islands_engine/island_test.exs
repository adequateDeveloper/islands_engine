defmodule IslandsEngine.IslandTest do
  use ExUnit.Case, async: true
  alias IslandsEngine.{Coordinate, Island, IslandTypes}

  # doctest Island

  test "new/2 constructs valid island types" do
    {:ok, starting_coordinate} = Coordinate.new 1, 1
    assert {:ok, _dot_island}     = Island.new :dot, starting_coordinate
    assert {:ok, _atoll_island}   = Island.new :atoll, starting_coordinate
    assert {:ok, _square_island}  = Island.new :square, starting_coordinate
    assert {:ok, _l_shape_island} = Island.new :l_shape, starting_coordinate
    assert {:ok, _s_shape_island} = Island.new :s_shape, starting_coordinate
  end

  test "new/2 prevents construction of island using invalid island type" do
    {:ok, coordinate} = Coordinate.new 4, 4
    assert {:error, :invalid_island_type} = Island.new :unknown, coordinate
  end

  test "new/2 prevents construction of island using invalid coordinates" do
    {:ok, starting_coordinate} = Coordinate.new 10, 10
    assert {:error, :invalid_coordinate} = Island.new :square, starting_coordinate
  end

  test "do islands overlap?" do
    {:ok, square_coordinate} = Coordinate.new 1, 1
    {:ok, square_island} = Island.new :square, square_coordinate

    {:ok, dot_coordinate} = Coordinate.new 1, 2
    {:ok, dot_island} = Island.new :dot, dot_coordinate

    {:ok, l_shape_coordinate} = Coordinate.new 5, 5
    {:ok, l_shape_island} = Island.new :dot, l_shape_coordinate

    assert Island.overlaps? square_island, dot_island
    refute Island.overlaps? square_island, l_shape_island
    refute Island.overlaps? dot_island, l_shape_island
  end

  test "a guess misses the island" do
    {:ok, dot_coordinate} = Coordinate.new 4, 4
    {:ok, guess_coordinate} = Coordinate.new 2, 2
    {:ok, dot_island} = Island.new  :dot, dot_coordinate
    assert :miss = Island.guess dot_island, guess_coordinate
  end

  test "a guess hits the island" do
    {:ok, square_coordinate} = Coordinate.new 1, 1
    {:ok, guess_coordinate} = Coordinate.new 1, 2
    {:ok, square_island} = Island.new :square, square_coordinate
    assert {:hit, square_island} = Island.guess square_island, guess_coordinate
    refute Island.forested? square_island
  end

  test "is the island forested?" do
    {:ok, dot_coordinate} = Coordinate.new 4, 4
    {:ok, good_guess_coordinate} = Coordinate.new 4, 4
    {:ok, bad_guess_coordinate} = Coordinate.new 5, 5
    {:ok, dot_island} = Island.new :dot, dot_coordinate

    assert :miss = Island.guess dot_island, bad_guess_coordinate
    refute Island.forested? dot_island

    {:hit, dot_island} = Island.guess dot_island, good_guess_coordinate
    assert Island.forested? dot_island
  end

  test "add_coordinates/2 constructs valid square type island" do
    expected_square_island = MapSet.new([
      %Coordinate{col: 1, row: 1},
      %Coordinate{col: 1, row: 2},
      %Coordinate{col: 2, row: 1},
      %Coordinate{col: 2, row: 2}
    ])

    offsets = IslandTypes.offsets :square
    {:ok, upper_left_coordinate} = Coordinate.new 1, 1

    actual_square_island = Island.add_coordinates offsets, upper_left_coordinate

    assert expected_square_island ==actual_square_island
  end

  test "add_coordinates/2 prevents construction of invalid square type island" do
    offsets = IslandTypes.offsets :square
    {:ok, upper_left_coordinate} = Coordinate.new 10, 10

    assert {:error, :invalid_coordinate} = Island.add_coordinates offsets, upper_left_coordinate
  end

  test "add_coordinates/2 constructs valid dot type island" do
    expected_dot_island = MapSet.new([%Coordinate{col: 1, row: 1}])

    offsets = IslandTypes.offsets :dot
    {:ok, upper_left_coordinate} = Coordinate.new 1, 1

    actual_dot_island = Island.add_coordinates offsets, upper_left_coordinate

    assert expected_dot_island == actual_dot_island
  end

  test "add_coordinates/2 prevents construction of invalid dot type island" do
    offsets = [{1, 1}]
    {:ok, upper_left_coordinate} = Coordinate.new 10, 10

    assert {:error, :invalid_coordinate} = Island.add_coordinates offsets, upper_left_coordinate
  end

  test "add_coordinate/3 constructs valid island coordinate" do
    island_coordinates = MapSet.new
    assert match? %MapSet{}, island_coordinates

    {:ok, starting_coordinate} = Coordinate.new 1, 1
    row_column_offset = {1, 1}

    assert {:cont, island_coordinates} = Island.add_coordinate island_coordinates, starting_coordinate, row_column_offset

    assert MapSet.member? island_coordinates, %Coordinate{col: 2, row: 2}
  end

  test "add_coordinate/3 prevents construction of invalid island coordinate" do
    island_coordinates = MapSet.new
    {:ok, starting_coordinate} = Coordinate.new 10, 10
    row_column_offset = {1, 1}

    assert {:halt, {:error, :invalid_coordinate}} = Island.add_coordinate island_coordinates, starting_coordinate, row_column_offset
  end

end
