defmodule IslandsEngine.CoordinateTest do
  use ExUnit.Case, async: true
  use PropCheck

  alias IslandsEngine.Coordinate

  doctest Coordinate

  # NOTE: Compare each unit test with its equivilent property test
  
  test "new Coordinate with invalid values returns error tuple" do
    assert {:error, :invalid_coordinate} == Coordinate.new 1, 11
    assert {:error, :invalid_coordinate} == Coordinate.new 11, 1
    assert {:error, :invalid_coordinate} == Coordinate.new 1, 0
    assert {:error, :invalid_coordinate} == Coordinate.new 0, 1
    assert {:error, :invalid_coordinate} == Coordinate.new -1, 10
    assert {:error, :invalid_coordinate} == Coordinate.new 10, -1
  end

  # by default generates 100 tests
  property "new Coordinate with invalid values returns error tuple" do
    forall {col_value, row_value} <- {(range -10, 0), range(11, 20)} do
      {:error, :invalid_coordinate} == Coordinate.new row_value, col_value
    end
  end

  test "new Coordinate with valid values returns ok tuple" do
    assert {:ok, %Coordinate{col: 10, row: 1}} == Coordinate.new 1, 10
  end

  # by default generates 100 tests
  property "new Coordinate with valid values returns ok tuple" do
    forall {col_value, row_value} <- {(range 1, 10), range(1, 10)} do
      coordinate(col_value, row_value) == Coordinate.new row_value, col_value
    end
  end

  defp coordinate(col_value, row_value) do
    {:ok, %Coordinate{col: col_value, row: row_value}}
  end

end
