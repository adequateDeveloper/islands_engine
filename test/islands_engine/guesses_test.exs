defmodule IslandsEngine.GuessesTest do
  use ExUnit.Case, async: true
  alias IslandsEngine.{Coordinate, Guesses}

  doctest Guesses

  setup do
    {:ok, hit_coordinate} = Coordinate.new 8, 3
    {:ok, miss_coordinate} = Coordinate.new 9, 7
    {:ok, hit_coordinate: hit_coordinate, miss_coordinate: miss_coordinate}
  end

  test "new Guesses returns correct data structure" do
    assert %Guesses{hits: %MapSet{}, misses: %MapSet{}} == Guesses.new
  end

  test "adding a hit updates Guesses correctly", %{hit_coordinate: hit} do
    guesses = Guesses.new
    guesses = Guesses.add guesses, :hit, hit
    assert MapSet.member? guesses.hits, hit
    assert 0 == MapSet.size guesses.misses
  end

  test "adding the same hit multiple times updates Guesses correctly", %{hit_coordinate: hit} do
    guesses = Guesses.new
    guesses = Guesses.add guesses, :hit, hit
    guesses = Guesses.add guesses, :hit, hit
    assert 1 == MapSet.size guesses.hits
  end

  test "adding a miss updates Guesses correctly", %{miss_coordinate: miss} do
    guesses = Guesses.new
    guesses = Guesses.add guesses, :miss, miss
    assert MapSet.member? guesses.misses, miss
    assert 0 == MapSet.size guesses.hits
  end

  test "adding the same miss multiple times updates Guesses correctly", %{miss_coordinate: miss} do
    guesses = Guesses.new
    guesses = Guesses.add guesses, :miss, miss
    guesses = Guesses.add guesses, :miss, miss
    assert 1 == MapSet.size guesses.misses
  end

  test "adding an :unknown prevents Guesses being updated", %{miss_coordinate: miss} do
    guesses = Guesses.new
    assert {:error, :invalid_guess} == Guesses.add guesses, :unknown, miss
    assert 0 == MapSet.size guesses.hits
    assert 0 == MapSet.size guesses.misses
  end

end
