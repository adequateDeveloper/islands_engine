defmodule IslandsEngine.IslandTypesTest do
  use ExUnit.Case, async: true
  alias IslandsEngine.IslandTypes, as: Types

  doctest Types

  test "the island types" do
    expected_types = [:atoll, :dot, :l_shape, :s_shape, :square]
    assert expected_types == Types.types
  end

  test "island type offsets" do
    assert [{0, 0}] == Types.offsets :dot
    assert [{0, 0}, {1, 0}, {2, 0}, {2, 1}] == Types.offsets :l_shape
    assert [{0, 0}, {0, 1}, {1, 0}, {1, 1}] == Types.offsets :square
    assert [{0, 1}, {0, 2}, {1, 0}, {1, 1}] == Types.offsets :s_shape
    assert [{0, 0}, {0, 1}, {1, 1}, {2, 0}, {2, 1}] == Types.offsets :atoll
    assert {:error, :invalid_island_type} == Types.offsets :unknown
  end

end
