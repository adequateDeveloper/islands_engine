defmodule IslandsEngine.BoardTest do
  use ExUnit.Case, async: true
  alias IslandsEngine.{Board, Coordinate, Island}

  # doctest Board

  setup do
    game_board = Board.new

    {:ok, dot_coordinate} = Coordinate.new 2, 2
    {:ok, dot_at_2} = Island.new :dot, dot_coordinate

    {:ok, dot_coordinate} = Coordinate.new 3, 3
    {:ok, dot_at_3} = Island.new :dot, dot_coordinate

    {:ok, square_coordinate} = Coordinate.new 1, 1
    {:ok, square_at_1} = Island.new :square, square_coordinate

    {:ok, board: game_board, square1: square_at_1, dot2: dot_at_2, dot3: dot_at_3}
  end

  test "a game board with island is constructed", %{board: board, square1: square_at_1} do
    game_board = Board.position_island board, :square, square_at_1

    assert Map.has_key? game_board, :square
    assert %Island{} = Map.get game_board, :square
    refute Board.all_islands_positioned? game_board
  end

  test "a game board prevents overlapping islands", %{board: board, square1: square_at_1, dot2: dot_at_2} do
    game_board = Board.position_island board, :square, square_at_1

    assert {:error, :overlapping_island} = Board.position_island game_board, :dot, dot_at_2
    refute Map.has_key? game_board, :dot
    refute Board.all_islands_positioned? game_board
  end

  test "a game board with multiple islands is constructed", %{board: board, square1: square_at_1, dot3: dot_at_3} do
    game_board = Board.position_island board, :square, square_at_1
    game_board = Board.position_island game_board, :dot, dot_at_3

    assert [:dot, :square] = Map.keys game_board
    refute Board.all_islands_positioned? game_board
  end

  test "a game board correctly reports a missed guess", %{board: board, square1: square_at_1, dot3: dot_at_3} do
    game_board = Board.position_island board, :square, square_at_1
    game_board = Board.position_island game_board, :dot, dot_at_3

    refute Board.all_islands_positioned? game_board

    {:ok, miss_coordinate} = Coordinate.new 10, 10
    assert {:miss, :none, :no_win, _game_board} = Board.guess game_board, miss_coordinate
  end

  test "a game board correctly reports a hit guess", %{board: board, square1: square_at_1} do
    game_board = Board.position_island board, :square, square_at_1

    refute Board.all_islands_positioned?(game_board)

    {:ok, hit_coordinate} = Coordinate.new(1, 1)
    assert {:hit, :none, :no_win, _game_board} = Board.guess game_board, hit_coordinate
  end

  test "a game board island has been hit and the game won", %{board: board, dot3: dot_at_3} do
    game_board = Board.position_island board, :dot, dot_at_3

    refute Board.all_islands_positioned? game_board

    {:ok, win_coordinate} = Coordinate.new(3, 3)
    assert {:hit, :dot, :win, _game_board} = Board.guess game_board, win_coordinate
  end

  test "check_all_islands/2 on a hit responds correctly", %{board: board} do
    {:ok, dot_coordinate} = Coordinate.new 3, 3
    {:ok, dot_island} = Island.new :dot, dot_coordinate

    game_board = Map.put(board, :dot, dot_island)

    {:dot, an_island} = Board.check_all_islands game_board, dot_coordinate
    assert MapSet.member?(an_island.hit_coordinates, dot_coordinate)
  end

  test "check_all_islands/2 on a miss responds correctly", %{board: board} do
    {:ok, dot_coordinate} = Coordinate.new 3, 3
    {:ok, bad_dot_coordinate} = Coordinate.new 5, 5
    {:ok, dot_island} = Island.new :dot, dot_coordinate

    game_board = Map.put(board, :dot, dot_island)

    assert :miss == Board.check_all_islands game_board, bad_dot_coordinate
  end

  test "guess_response/2 on a hit responds correctly", %{board: board, dot3: dot_at_3} do
    game_board = Map.put(board, :dot, dot_at_3)

    assert {:hit, :none, :no_win, game_board} == Board.guess_response {:dot, dot_at_3}, game_board
  end

  test "guess_response/2 on a hit, forested, win responds correctly", %{board: board} do
    {:ok, dot_coordinate} = Coordinate.new 3, 3
    {:ok, dot_island} = Island.new :dot, dot_coordinate
    {:hit, dot_island} = Island.guess dot_island, dot_coordinate

    game_board = Map.put(board, :dot, dot_island)

    assert {:hit, :dot, :win, game_board} == Board.guess_response {:dot, dot_island}, game_board
  end

  test "guess_response/2 on a miss responds correctly" do
    assert {:miss, :none, :no_win, _game_board} = Board.guess_response :miss, Board.new
  end

  test "forest_check/2 evaluates island type as forested" do
    {:ok, dot_coordinate} = Coordinate.new 3, 3
    {:ok, dot_island} = Island.new :dot, dot_coordinate
    {:hit, dot_island} = Island.guess dot_island, dot_coordinate

    game_board = Board.new
    game_board = Map.put(game_board, :dot, dot_island)

    assert :dot == Board.forest_check game_board, :dot
  end

  test "forest_check/2 evaluates island type as not forested" do
    {:ok, dot_coordinate} = Coordinate.new 3, 3
    {:ok, bad_guess_coordinate} = Coordinate.new 5, 5
    {:ok, dot_island} = Island.new :dot, dot_coordinate
    :miss = Island.guess dot_island, bad_guess_coordinate

    game_board = Board.new
    game_board = Map.put(game_board, :dot, dot_island)

    assert :none == Board.forest_check game_board, :dot
  end

  test "win_check/1 evaluates game as won" do
    {:ok, dot_coordinate} = Coordinate.new 3, 3
    {:ok, dot_island} = Island.new :dot, dot_coordinate
    {:hit, dot_island} = Island.guess dot_island, dot_coordinate

    game_board = Board.new
    game_board = Map.put(game_board, :dot, dot_island)

    assert :win == Board.win_check game_board
  end

  test "win_check/1 evaluates game as not won" do
    {:ok, dot_coordinate} = Coordinate.new 3, 3
    {:ok, bad_guess_coordinate} = Coordinate.new 5, 5
    {:ok, dot_island} = Island.new :dot, dot_coordinate
    :miss = Island.guess dot_island, bad_guess_coordinate

    game_board = Board.new
    game_board = Map.put(game_board, :dot, dot_island)

    assert :no_win == Board.win_check game_board
  end

  test "all_forested?/1 evaluates board as forested" do
    {:ok, dot_coordinate} = Coordinate.new 3, 3
    {:ok, dot_island} = Island.new :dot, dot_coordinate
    {:hit, dot_island} = Island.guess dot_island, dot_coordinate

    game_board = Board.new
    game_board = Map.put(game_board, :dot, dot_island)

    assert Board.all_forested? game_board
  end

  test "all_forested?/1 evaluates board as not forested" do
    {:ok, dot_coordinate} = Coordinate.new 3, 3
    {:ok, bad_guess_coordinate} = Coordinate.new 5, 5
    {:ok, dot_island} = Island.new :dot, dot_coordinate
    :miss = Island.guess dot_island, bad_guess_coordinate

    game_board = Board.new
    game_board = Map.put(game_board, :dot, dot_island)

    refute Board.all_forested? game_board
  end

  test "forested?/2 evaluates island as forested" do
    {:ok, dot_coordinate} = Coordinate.new 3, 3
    {:ok, dot_island} = Island.new :dot, dot_coordinate
    {:hit, dot_island} = Island.guess dot_island, dot_coordinate

    game_board = Board.new
    game_board = Map.put(game_board, :dot, dot_island)

    assert Board.forested? game_board, :dot
  end

  test "forested?/2 evaluates island as not forested" do
    {:ok, dot_coordinate} = Coordinate.new 3, 3
    {:ok, bad_guess_coordinate} = Coordinate.new 5, 5
    {:ok, dot_island} = Island.new :dot, dot_coordinate
    :miss = Island.guess dot_island, bad_guess_coordinate

    game_board = Board.new
    game_board = Map.put(game_board, :dot, dot_island)

    refute Board.forested? game_board, :dot
  end

  test "overlaps_existing_island?/3 prevents overlapping islands" do
    {:ok, square_coordinate} = Coordinate.new 1, 1
    {:ok, square_island} = Island.new :square, square_coordinate
    game_board = Board.new
    game_board = Map.put(game_board, :square, square_island)

    {:ok, dot_coordinate} = Coordinate.new 2, 2
    {:ok, dot_island} = Island.new :dot, dot_coordinate

    assert Board.overlaps_existing_island? game_board, :dot, dot_island
  end

  test "overlaps_existing_island?/3 allows non-overlapping islands" do
    {:ok, square_coordinate} = Coordinate.new 1, 1
    {:ok, square_island} = Island.new :square, square_coordinate
    game_board = Board.new
    game_board = Map.put(game_board, :square, square_island)

    {:ok, dot_coordinate} = Coordinate.new 3, 3
    {:ok, dot_island} = Island.new :dot, dot_coordinate

    refute Board.overlaps_existing_island? game_board, :dot, dot_island
  end

 test "overlaps_existing_island?/3 tests for duplicate key" do
   {:ok, dot_coordinate} = Coordinate.new 1, 1
   {:ok, dot_island} = Island.new :dot, dot_coordinate
   game_board = Board.new
   game_board = Map.put(game_board, :dot, dot_island)

   {:ok, another_dot_coordinate} = Coordinate.new 3, 3
   {:ok, another_dot_island} = Island.new :dot, another_dot_coordinate

   refute Board.overlaps_existing_island? game_board, :dot, another_dot_island
 end

end
