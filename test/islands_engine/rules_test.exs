defmodule IslandsEngine.RulesTest do
  use ExUnit.Case, async: true
  alias IslandsEngine.Rules

  doctest Rules

  setup do
    {:ok, new_rules: Rules.new}
  end

  test "transition through all states via all events", %{new_rules: rules} do
    # :initialized state
    assert :initialized == rules.state
    assert :islands_not_set == rules.player1
    assert :islands_not_set == rules.player2

    # :players_set state via :add_player event
    {:ok, new_rules} = Rules.check rules, :add_player
    assert :players_set == new_rules.state
    assert :islands_not_set == new_rules.player1
    assert :islands_not_set == new_rules.player2

    # :players_set state unchanged during :position_islands events by players
    {:ok, new_rules} = Rules.check new_rules, {:position_islands, :player1}
    {:ok, new_rules} = Rules.check new_rules, {:position_islands, :player2}
    assert :players_set == new_rules.state
    assert :islands_not_set == new_rules.player1
    assert :islands_not_set == new_rules.player2

    # :islands_set state via player :set_island event after which island positioning not allowed for player
    {:ok, new_rules} = Rules.check new_rules, {:set_islands, :player1}
    assert :players_set == new_rules.state
    assert :islands_set == new_rules.player1
    assert :islands_not_set == new_rules.player2
    assert :error == Rules.check new_rules, {:position_islands, :player1}
    refute :error == Rules.check new_rules, {:position_islands, :player2}

    # :player1_turn state via both players :set_island event
    {:ok, new_rules} = Rules.check new_rules, {:set_islands, :player2}
    assert :player1_turn == new_rules.state
    assert :islands_set == new_rules.player1
    assert :islands_set == new_rules.player2
    assert :error == Rules.check new_rules, {:position_islands, :player2}

    # when in :player1_turn state no prior state transitions are allowed
    assert :error == Rules.check new_rules, :add_player
    assert :error == Rules.check new_rules, {:position_islands, :player1}
    assert :error == Rules.check new_rules, {:position_islands, :player2}
    assert :error == Rules.check new_rules, {:set_islands, :player1}
    assert :error == Rules.check new_rules, {:set_islands, :player2}

    # players alternate guessing coordinates, beginning with player1. If :player2 tries to guess first, that should be
    # an error.
    assert :error == Rules.check new_rules, {:guess_coordinate, :player2}
    assert :player1_turn == new_rules.state

    {:ok, new_rules} = Rules.check new_rules, {:guess_coordinate, :player1}
    assert :player2_turn == new_rules.state
    assert :islands_set == new_rules.player1
    assert :islands_set == new_rules.player2

    {:ok, new_rules} = Rules.check new_rules, {:guess_coordinate, :player2}
    assert :player1_turn == new_rules.state
    assert :islands_set == new_rules.player1
    assert :islands_set == new_rules.player2

    # Any guess that doesn’t result in a win should not transition the state.
    {:ok, new_rules} = Rules.check new_rules, {:win_check, :no_win}
    assert :player1_turn == new_rules.state
    assert :islands_set == new_rules.player1
    assert :islands_set == new_rules.player2

    {:ok, new_rules} = Rules.check new_rules, {:guess_coordinate, :player1}
    {:ok, new_rules} = Rules.check new_rules, {:win_check, :no_win}
    assert :player2_turn == new_rules.state
    assert :islands_set == new_rules.player1
    assert :islands_set == new_rules.player2

    # Any guess that results in a win should transition the state to :game_over
    {:ok, new_rules} = Rules.check new_rules, {:win_check, :win}
    assert :game_over == new_rules.state
    assert :islands_set == new_rules.player1
    assert :islands_set == new_rules.player2
  end

  test "new rules is initialized", %{new_rules: rules} do
    assert :initialized = rules.state
    assert :islands_not_set = rules.player1
    assert :islands_not_set = rules.player2
  end

  test ":add_player event transitions state from :initialized to :players_set", %{new_rules: rules} do
    assert :initialized == rules.state

    {:ok, new_rules} = Rules.check rules, :add_player

    assert :players_set == new_rules.state
    assert :islands_not_set = new_rules.player1
    assert :islands_not_set = new_rules.player2
  end

  test ":position_islands events for players does not transition state from :players_set", %{new_rules: rules} do
    {:ok, new_rules} = Rules.check rules, :add_player

    {:ok, new_rules} = Rules.check new_rules, {:position_islands, :player1}
    assert :players_set == new_rules.state

    {:ok, new_rules} = Rules.check new_rules, {:position_islands, :player2}
    assert :players_set == new_rules.state

    assert :islands_not_set = rules.player1
    assert :islands_not_set = rules.player2
  end

  test ":island_set event for first player transitions their player state", %{new_rules: rules} do
    {:ok, new_rules} = Rules.check rules, :add_player
    {:ok, new_rules} = Rules.check new_rules, {:position_islands, :player2}
    assert :players_set == new_rules.state
    assert :islands_not_set == new_rules.player2

    {:ok, new_rules} = Rules.check new_rules, {:set_islands, :player2}
    assert :players_set == new_rules.state
    assert :islands_set == new_rules.player2

    assert :error == Rules.check new_rules, {:position_islands, :player2}
    assert :players_set == new_rules.state
    assert :islands_set == new_rules.player2
  end

  test ":island_set event for second player transitions their player state and state", %{new_rules: rules} do
    assert :initialized == rules.state

    {:ok, new_rules} = Rules.check rules, :add_player
    assert :players_set == new_rules.state

    {:ok, new_rules} = Rules.check new_rules, {:set_islands, :player2}
    assert :islands_set == new_rules.player2
    assert :players_set == new_rules.state

    {:ok, new_rules} = Rules.check new_rules, {:set_islands, :player1}
    assert :islands_set == new_rules.player1
    assert :player1_turn == new_rules.state

    assert :error == Rules.check new_rules, {:position_islands, :player2}
    assert :error == Rules.check new_rules, {:position_islands, :player1}
  end

  test "when state in :player1_turn no prior state transitions are allowed", %{new_rules: rules} do
    {:ok, new_rules} = Rules.check rules, :add_player
    {:ok, new_rules} = Rules.check new_rules, {:set_islands, :player2}
    {:ok, new_rules} = Rules.check new_rules, {:set_islands, :player1}

    assert :player1_turn == new_rules.state

    assert :error == Rules.check new_rules, :add_player
    assert :error == Rules.check new_rules, {:position_islands, :player1}
    assert :error == Rules.check new_rules, {:position_islands, :player2}
    assert :error == Rules.check new_rules, {:set_islands, :player1}
    assert :error == Rules.check new_rules, {:set_islands, :player2}
  end

  test "transition state from :player1_turn to :player2_turn with event :guess_coordinate", %{new_rules: rules} do
    {:ok, new_rules} = Rules.check rules, :add_player
    {:ok, new_rules} = Rules.check new_rules, {:set_islands, :player1}
    {:ok, new_rules} = Rules.check new_rules, {:set_islands, :player2}

    {:ok, new_rules} = Rules.check new_rules, {:guess_coordinate, :player1}

    assert :player2_turn == new_rules.state
  end

  test "transition state from :player2_turn to :player1_turn with event :guess_coordinate", %{new_rules: rules} do
    {:ok, new_rules} = Rules.check rules, :add_player
    {:ok, new_rules} = Rules.check new_rules, {:set_islands, :player1}
    {:ok, new_rules} = Rules.check new_rules, {:set_islands, :player2}

    {:ok, new_rules} = Rules.check new_rules, {:guess_coordinate, :player1}
    {:ok, new_rules} = Rules.check new_rules, {:guess_coordinate, :player2}

    assert :player1_turn == new_rules.state
  end

  test "transition state from :player1_turn to :game_over - :win for player1", %{new_rules: rules} do
    {:ok, new_rules} = Rules.check rules, :add_player
    {:ok, new_rules} = Rules.check new_rules, {:set_islands, :player1}
    {:ok, new_rules} = Rules.check new_rules, {:set_islands, :player2}

    assert :player1_turn == new_rules.state

    {:ok, new_rules} = Rules.check new_rules, {:win_check, :win}
    assert :game_over == new_rules.state
  end

  test "transition state from :player2_turn to :game_over - :win for player2", %{new_rules: rules} do
    {:ok, new_rules} = Rules.check rules, :add_player
    {:ok, new_rules} = Rules.check new_rules, {:set_islands, :player1}
    {:ok, new_rules} = Rules.check new_rules, {:set_islands, :player2}

    {:ok, new_rules} = Rules.check new_rules, {:guess_coordinate, :player1}
    assert :player2_turn == new_rules.state

    {:ok, new_rules} = Rules.check new_rules, {:win_check, :win}
    assert :game_over == new_rules.state
  end

  test "state unchanged from :player1_turn for event :no_win for player1", %{new_rules: rules} do
    {:ok, new_rules} = Rules.check rules, :add_player
    {:ok, new_rules} = Rules.check new_rules, {:set_islands, :player1}
    {:ok, new_rules} = Rules.check new_rules, {:set_islands, :player2}

    assert :player1_turn == new_rules.state

    {:ok, new_rules} = Rules.check new_rules, {:win_check, :no_win}
    assert :player1_turn == new_rules.state
  end

  test "state unchanged from :player2_turn for event :no_win for player2", %{new_rules: rules} do
    {:ok, new_rules} = Rules.check rules, :add_player
    {:ok, new_rules} = Rules.check new_rules, {:set_islands, :player1}
    {:ok, new_rules} = Rules.check new_rules, {:set_islands, :player2}

    {:ok, new_rules} = Rules.check new_rules, {:guess_coordinate, :player1}
    assert :player2_turn == new_rules.state

    {:ok, new_rules} = Rules.check new_rules, {:win_check, :no_win}
    assert :player2_turn == new_rules.state
  end

  test "unknown event produces error and leaves state unchanged", %{new_rules: rules} do
    assert :initialized == rules.state
    assert :error == Rules.check rules, :unknown_event
    assert :initialized == rules.state
  end

  test "check/2 when state in :initialized and event is :add_player", %{new_rules: rules} do
    assert :initialized == rules.state
    assert :islands_not_set == rules.player1
    assert :islands_not_set == rules.player2

    {:ok, new_rules} = Rules.check rules, :add_player

    assert :players_set == new_rules.state
    assert :islands_not_set == new_rules.player1
    assert :islands_not_set == new_rules.player2
  end

  test "check/2 when state in :players_set and event is :position_islands", %{new_rules: rules} do
    {:ok, new_rules} = Rules.check rules, :add_player

    assert :players_set == new_rules.state
    assert :islands_not_set == new_rules.player1
    assert :islands_not_set == new_rules.player2

    Rules.check new_rules, {:position_islands, :player1}
    Rules.check new_rules, {:position_islands, :player2}

    assert :players_set == new_rules.state
    assert :islands_not_set == new_rules.player1
    assert :islands_not_set == new_rules.player2
  end

  test "check/2 when state and event are unknown" do
    assert :error == Rules.check nil, nil
  end

  test "both_players_islands_set?/1", %{new_rules: rules} do
    refute Rules.both_players_islands_set?(rules)

    {:ok, new_rules} = Rules.check rules, :add_player
    {:ok, new_rules} = Rules.check new_rules, {:set_islands, :player2}

    refute Rules.both_players_islands_set?(new_rules)

    {:ok, new_rules} = Rules.check new_rules, {:set_islands, :player1}

    assert Rules.both_players_islands_set?(new_rules)
  end

end
