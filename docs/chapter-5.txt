pg 97:
Chapter 5 Process Supervision for Recovery
------------------------------------------
What we’ll do in this chapter
• look at linking process and trapping exits
• examine the strategies for process supervision
• build a custom supervisor
• restore game state after a crash


We all work with computers. We know it’s inevitable that things will go wrong,
sometimes really wrong. In spite of our best laid plans, state goes bad, systems
raise exceptions, and servers crash. These failures often seem to come out of
nowhere, unpredictably and without warning.

To combat these inevitable problems, we need to boost fault tolerance. We
need to isolate failures as much as possible, handle them, and have the system
as a whole carry on.

Elixir and OTP provide a world-class mechanism for handling problems and
moving on: process supervision. Process supervision means we can have spe-
cialized processes that watch other processes, and restart them when they crash.

We’re going to build our own supervisor for the Game module. We’ll make sure
it starts a new process when we start the game engine. We’ll use that super-
visor process to start and supervise each game process.

pg 98:
Fault tolerance

Erlang and Elixir have reputations for tremendous fault tolerance. This is
well deserved, but not because they prevent errors. Instead they give us the
tools to recover gracefully from any errors that crop up at runtime.

Almost all languages, including Elixir, have built-in mechanisms to handle
exceptions. These require that we identify risky code in advance, wrap it in
a block that tries to execute it, and provide a block to rescue the situation if
the code fails. In most languages, this kind of exception handling is essential,
but in Elixir we hardly ever have to reach for it.

The OTP team at Ericsson took an interesting tack around this pattern. For
them, extreme fault tolerance wasn’t a “nice to have.” It was a critical
requirement of the language. Telephone utilities have very stringent uptime
requirements. The phones need to work no matter what, even in the event of
a natural disaster.

The team reasoned that it’s nearly impossible to predict all possible failures
in advance, so they decided to focus on recovering from failure instead. They
wanted to code for the happy path and have a separate mechanism get things
back on track when the inevitable errors happen.

The design they came up with is the supervisor Behaviour. It extracts error
handling code from business logic into its own modules. Supervisor modules
spawn supervisor processes that link to other processes and watch for failure,
restarting those linked processes if they crash.

This separation of concerns makes our code clearer and easier to maintain.
It keeps our business logic free of diversions for handling exceptions. We end
up writing more confident code that assumes success, but supervisors always
have our back when things go wrong.

The supervisor Behaviour is based on ideas that build on and reinforce each
other:
  1) Most runtime errors are transient and happen because of bad state.

  2) The best way to fix bad state is to let the process crash and restart it with
     good state.

  3) Restarts works best on systems like the BEAM that have small, independent
     processes. Having independent processes lets us isolate errors to the smallest
     area possible, minimizing any disruption during restarts.

We’re going to look at process supervision from all angles. We’ll start with the
different ways we can spawn new processes and the implications that has for
how processes interact. That will lead us to the way supervisors interact with
the processes they supervise. We’ll see the different strategies supervisors
can use for restarting crashed processes, and then we’ll talk about different
ways of recovering state after processes restart.

To illustrate how process supervision works, let’s start with the different ways
we can spawn processes and the effect those different ways have on the way
processes interact.

pg 99:
Linking Processes

Supervisors may sound magical, but there are two simple, practical mecha-
nisms behind their magic: linking processes and trapping exits. We’re going
to see how they work together to make supervisors work.

The key to process supervision is for one process to be able to link to other
processes and know when those other processes crash. Beyond that, the
supervising process needs to be able to keep running if it gets crash notifica-
tions from processes it’s linked to.

When one process in a group of linked processes crashes, it sends a special
kind of message called an exit signal to all the other processes it’s linked to. Any
process that receives an exit signal will also exit and send an exit signal to all
the other processes that it is linked to, forming a kind of chain reaction of exits.

This chain reaction makes it hard to fulfill one of the main ideas behind
supervisor processes—which is not to crash—when one of the processes
they’re monitoring does crash.

The mechanism to fix this is called trapping exits. When a process traps exits,
it transforms exit signals into regular messages that end up in the process’s
mailbox. That process can then handle those regular messages just as it
would any other. That’s the key to making supervisors work.

pg 102:
There are three categories of reasons for a process to terminate: :normal , :kill ,
and any other failure. A normal termination won’t send an exit signal, so it
won’t terminate any other linked processes. :kill and any other reason will
cause the process to send an exit signal.

pg 103:
When one of a group of linked processes crashes, it sends an exit signal to
all the other processes that it’s linked to, and they will all crash as well.

Trapping Exits

By default, when a process receives an exit signal with any reason other than
:normal , the process exits and propagates that exit signal to any other process
it is linked to.

When :trap_exit is set to true, the process converts exit signals to messages of
the form {:EXIT, from, reason} , which the process can handle like any ordinary
message.

Supervisors handle all this behind the scenes for us. By implementing a
supervisor, we’ll eliminate the need to manually trap exits and link to other
processes, but it’s useful to know what they’re doing on our behalf.

pg 105:
Introducing the Supervisor Behaviour

Process supervision begins with the supervisor Behaviour. You’ll get an
overview of it in this section. Over the next few sections, you’ll see how to
customize supervisor processes. Then we’ll build a supervisor to monitor all
the game processes in Islands.

Like GenServer , supervisor is an Elixir Behaviour that wraps an OTP Behaviour.
In this case, that is :supervisor . Also like GenServer , we can spawn new processes
from supervisor modules. These supervisor processes will do all the starting,
monitoring, restarting, and stopping of other processes within an application.

We call any process under supervision a child process of the supervisor. Child
processes can be workers, like ‘GenServer‘ processes, or other supervisor
processes.

Since supervisors can supervise other supervisors, we can build tree structures
of supervised processes. These supervision trees allow us to be specific about
how we supervise processes. By specifying different startup options, we can
tailor the way a supervisor behaves for a given type of process.

When we define a new supervisor module, we describe how we want it to
supervise its children, what restart strategy to use, and specify under which
circumstances the supervisor should restart the process.

We can also define some circumstances under which the supervisor itself
should terminate and restart. Consider the case of a process that crashes
continually and doesn’t successfully restart. There’s clearly something wrong
that restarts aren’t fixing, and we wouldn’t want those restarts to continue
indefinitely.

Supervisors allow us to configure the maximum allowable number of restarts
over a given period of time with the :max_restarts and :max_seconds options. The
default value for :max_restarts is 3, and the default value for :max_seconds is 5.

Supervision Strategies

Processes often depend on one another to do their work. If a child process
crashes, restarting just that one process might not stabilize the application.
In the event of a crash, a supervisor needs to decide whether to restart just
the one crashed process or other processes as well. If it needs to restart others,
it must decide which ones. Supervisors depend on restart strategies to make
those decisions.

pg 106:
Each supervisor process can supervise a number of child processes. Those
child processes might have different types of relationships with one another.

Sometimes all the child processes depend on one another, so if one of them
crashes, the others will be invalid. Sometimes they are all completely indepen-
dent, so if one crashes, the others will be fine.

Child processes all start in a temporal order. In some cases, each depends
on the processes that spawned before it, so that if one crashes, all the pro-
cesses that started after it will be invalid.

The pattern supervisors follow when starting child processes matters as well.
Some supervisors will start a fixed number of child processes at once, when
the application starts. Others will need to start and stop a variable number
of child processes at runtime.

Islands Engine falls into that second category. We’ll be starting and stopping Game
processes all the time.

All of these conditions have an impact on which strategy we choose. Let’s
take a closer look at the strategies next.

One for One

With the one-for-one strategy, if a single process terminates, the supervisor
will restart just that one process. This is best if the group of supervised pro-
cesses can work independently of one another—in other words, if restarting
the one process will not upset the functioning of the whole group of processes.

Let’s say we have one supervisor supervising three workers. Then one of those
workers crashes. The supervisor will restart only that one worker.

See: docs/screenshots/14-one-for-one-strategy.png

pg 107:
One for All

With the one-for-all strategy, if a single process terminates, the supervisor
will terminate all the rest of the supervised processes and restart them all.
This strategy is best if the group of processes do depend on each other, and
each other’s state, to work properly.

Imagine we have the same setup as before, and one worker crashes:

See: docs/screenshots/15-one-for-all-strategy.png

This time the supervisor will restart all the workers.

Rest for One

With the rest-for-one strategy, we need to look at a group of supervised pro-
cesses as having a temporal order: the order they were started in. Let’s assume
that order goes from earliest to latest, left to right. If a process in the middle
terminates, the supervisor will terminate all the processes that started after
the problem process; then it will restart all the terminated processes. This
strategy works best for groups of processes that have a temporal dependen-
cy—in other words, more newly spawned processes depend on the state and
integrity of the processes started before them.

See: docs/screenshots/16-rest-for-one-strategy.png

Simple One for One

The previous strategies are all fine, but they are not dynamic. They require
the supervisor to start the new processes when we start the BEAM.

Islands is different; we’ll be starting games and ending games all the time. For
that, there’s another strategy, simple one for one. This has a restart strategy
similar to one-for-one. If an individual process terminates, the supervisor will
restart just that one process.

See: docs/screenshots/17-simple-one-for-one-strategy.png

* * * Migrating from Supervisor's deprecated :simple_one_for_one to DynamicSupervisor * * *

A supervisor that starts children dynamically.

The Supervisor module was designed to handle mostly static children that are started 
in the given order when the supervisor starts. A DynamicSupervisor starts with no children. 
Instead, children are started on demand via start_child/2. When a dynamic supervisor terminates, 
all children are shut down at the same time, with no guarantee of ordering.

Islands Engine uses a DynamicSupervisor with one_for_one strategy.

See: lib/islands_engine/application.ex

See: https://hexdocs.pm/elixir/DynamicSupervisor.html

**********************************************************************************************

pg 108:
The Child Specification

The strategies describe a lot of how a supervisor will work with child processes,
but they are not the whole story. There are a number of other options we can
specify. Those options are stored in the child specification.

Fortunately, we don’t have too much work to do here. The GenServer Behaviour
creates a default child specification for us whenever we add the use GenServer
line to a module.

      $ iex -S mix
      iex> alias IslandsEngine.GameServer
      IslandsEngine.GameServer
      iex(2)> GameServer.child_spec "example"
      %{
         id: IslandsEngine.GameServer,
         start: {IslandsEngine.GameServer, :start_link, ["example"]}
      }

• :restart tells the supervisor whether it should restart its child processes.
:permanent means always restart the children. :temporary means never restart
the children. And :transient means restart them only if they crash.

• :start is a three-element tuple that tells the supervisor which module,
function, and arguments to use when starting or restarting child processes.

pg 110:
A Supervisor for the Game

*** NOTE: Now using the DynamicServer instead of the:simple_one_for_one strategy. ***

Each game in Islands is a separate GenServer process. These processes will
come and go as players start new games and then end them. We’ll need a
supervisor specifically to monitor games, and the :simple_one_for_one strategy is
perfect for processes that we need to start and stop at runtime.

See: lib/islands_engine/game_supervisor.ex

Just as 'use GenServer' created a child specification for us in the GameServer module,
'use DynamicSupervisor' created one for the GameSupervisor module. We can see that specifi-
cation with the child_spec/1 function.

$ iex -S mix
iex> alias IslandsEngine.{GameServer, GameSupervisor}
[IslandsEngine.GameServer, IslandsEngine.GameSupervisor]
iex> GameServer.child_spec :any_argument
%{
  id: IslandsEngine.GameServer,
  start: {IslandsEngine.GameServer, :start_link, [:any_argument]}
}
iex> GameSupervisor.child_spec :any_argument
%{
  id: IslandsEngine.GameSupervisor,
  start: {IslandsEngine.GameSupervisor, :start_link, [:any_argument]},
  type: :supervisor
}

This is where we gain more control over the supervisor. If we needed to, we
could pass in overrides to the child specification in the 'use DynamicSupervisor' line just
as we did for use GenServer in the GameServer module. We can also add public functions
to this module for starting and stopping child processes.

pg 112:
Starting the Supervision Tree

Supervisor processes can do their job only if they are running. We can’t know
in advance when a player is going to start a new game, so we need to make
sure that the GameSupervisor is running as soon as the application starts up.

When we generated the islands_engine project, we passed in the --sup flag to mix new. 
That automatically created an application.ex file for us, and the start/2 function in that file starts
a top-level supervisor process called IslandsEngine.Supervisor.

Currently, that supervisor process starts the Registry as a worker, but we’ll also
have it start a new GameSupervisor process whenever we start the application. We’ll
use that GameSupervisor process to start and supervise each new game.

pg 114:
Starting and Stopping Child Processes

Now we need to have GameSupervisor process start worker processes, which is to say games of Islands.

pg 116:
There’s another case we need to handle before we’re done with this section.
What happens if a player starts a game and then abandons it somewhere
along the way? We don’t want to let those processes just sit there taking up
system resources.

Fortunately, GenServer gives us an automated way to have processes time out
and shut themselves down if they haven’t received a new message in a given
number of milliseconds. All we need to do is add a fourth element to any of
the GenServer reply tuples: a positive integer representing the number of mil-
liseconds to wait before timing out {:reply, :some_reply, %{}, 1000} .

pg 119:
Recovering State After a Crash

This is where we really fulfill the promise of fault tolerance. It’s one thing to
restart a process if it crashes and then move on. It’s another thing entirely
to restart it and restore the last known good state.

The way we’ll do this is to save a copy of the data outside of the current pro-
cess, or any other process the current one is linked to. We’ll do this when we
initialize the process, and then again whenever the state changes.

Whenever we start a new process, or restart a crashed process, we’ll check
for that saved state. If it exists, that means we’re restarting, so we’ll use the
saved version. If it doesn’t exist, that means it’s a new process, so we’ll use
fresh state.

The storage engine we’ll use is ETS, which is short for Erlang Term Storage.
ETS comes with OTP, and it allows us to store data in in-memory tables as
two-element tuples. The first element of each tuple is the key; the second is
the value.

pg 121:
Step one is to make sure we have a table available whenever IslandsEngine
is running. We’ll need it to be public so that all game processes will have read
and write access. It should be a set so that we can have only one game state
per key. Let’s call the table :game_state.

We can make the table available by adding a call to :ets.new/2 in the start/2
function in lib/islands_engine/application.ex:
See: lib/islands_engine/application.ex

pg 121:
Storing and Retrieving Game State

We’ll want to store the full state for each game in the :game_state table whenever
it changes. We register each game process by the name of the first player, so
that name makes a great key to store the state under.

For game processes, some state will change whenever there is a successful
reply—either in the game itself or in the state machine data. Luckily, we wrote
a single function in the Game module to handle successful replies: reply_success/2 .
A call to :ets.insert/2 with the name of :player1 as the key and the full game state
as the value is all we need.
See: lib/islands_engine/game.ex

Any time we start or restart a process, GenServer will trigger the init/1 callback.
That makes init/1 a good place to check the :game_state table for any state stored
under the first player’s name.

If :ets.lookup/2 returns an empty list, we generate fresh state the way init/1 had
done before. If :ets.lookup/2 returns some state, we’ll use that instead.
See: lib/islands_engine/game_server.ex

pg 122:
Any time we start or restart a process, GenServer will trigger the init/1 callback.
That makes init/1 a good place to check the :game_state table for any state stored
under the first player’s name.

If :ets.lookup/2 returns an empty list, we generate fresh state the way init/1 had
done before. If :ets.lookup/2 returns some state, we’ll use that instead.

pg 123:
We removed the @timeout from the return tuple of init/1 , but we put it back here in handle_info/2.
This is now the last callback to return from the initial call to start_link/1 . There
won’t automatically be a new message in the game process’s mailbox right
after this, so the timeout will be able to do its job.

While this works, it does introduce a race condition. It’s important to under-
stand why we have a race condition and talk about the code we’ve written
that mitigates the risk.

We register game processes by name with the Registry . Other processes can
send messages to them with a :via tuple at any time—whether the PID exists
or not, and whether the state is properly reset after a crash or not. This means
it’s possible for another message to get in front of our :set_state message during
a restart. In Islands, this risk is low because of the small number of messages
sent to any single game.

The state machine offers a level of protection here as well. When init/1 returns,
it sets fresh state in the game, which means that the state is :initialized . The
only action we can take in that state is adding another player. Any other
action will return an error, effectively ignoring that message and bringing
the :set_state message to the top, or one step closer to the top, of the game’s
mailbox. This is not an ironclad guarantee, but we have minimized the
potential for harm.

pg 125:
Cleaning Up After a Game

We’re saving the state of each game in the :game_state table, but so far, we
haven’t removed that state when the game is over. :game_state will continue to
grow and use memory unless we delete a game’s key from the :game_state table
when the supervisor terminates the child process, or when the child process
times out.
See: lib/islands_engine/state_manager.ex
     lib/islands_engine/game_supervisor.ex stop_game/1

Cleaning up the data after a GenServer timeout is a little more involved, but
not much. One of the callbacks the GenServer Behaviour defines is terminate/2 .
This is the proper place to do any cleanup before the process exits, and it’s
where the call to :ets.delete/2 should go.

The wrinkle is that we shouldn’t call :ets.delete/2 every time a game process
terminates, but only when it times out. If a process crashes, we still want to
keep the data.

We’ve already got a clause of handle_info/2 that handles the :timeout message and
returns a tagged :stop tuple.

Tagging the return tuple with :stop causes the Behaviour to trigger the terminate/2
callback. It will pass the middle term of the return tuple in as the first argu-
ment so we can pattern match on it.

ETS is an in-memory data store. What happens when the BEAM’s memory goes away? 
What happens when the host reboots?

pg 127:
Data Durability

The code we’ve written so far will work great as long as the BEAM stays up. If
we restart the BEAM for any reason, though, the ETS table we created will be
gone along with all its data. To handle BEAM restarts, we need more durability.

The way to get that durability looks a lot like the way we’re recovering data
from ETS, just taken one step further. To recover state when a process restarts,
we store it outside of the process, or any processes linked to it. To recover state
after a BEAM restart, we need to store the state outside the BEAM.

The shape of the code we currently have will work for this. All the functions
are in place and working the way we want them to. We’ll just need to swap
out ETS for another storage mechanism.

This is where the number of available options grows enormously. What we can do is
look at some OTP options as well as a strategy that will apply broadly to a lot
of other options.

OTP offers two options right off the bat. DETS is a disk-based version of ETS. 1
DETS starts tables with :dets.open_file/2 instead of :ets.new/2 , but the query API
is remarkably similar to ETS. This is the durable option with the least change
to the existing code.

Stepping outside of OTP, a single broad strategy will work with a large number
of data stores. The idea is to convert the game state to JSON. Once the data
is in JSON, the data storage world is our oyster. We can store the game state
as a JSON data type practically anywhere.

pg 128:
Wrapping Up

What we’ve done in this chapter is really pretty remarkable. We started with
game processes that could crash and lose all their state at any time. We’ve
ended up with games that will recover from any crash and restore their state
to the last good version, automatically.

Try that in any other language.

Along the way, we’ve explored linking processes and trapping exits. We’ve
looked at ways to customize the way supervisors behave with restart strategies
and child specifications. We’ve also seen how to start and stop supervised
processes. As a bonus, we’ve gotten an introduction to ETS as well.

Investigate: https://hexdocs.pm/persistent_ets/PersistentEts.html