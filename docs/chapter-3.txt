Chapter 3
---------
pg 43:
Manage State with a State Machine
---------------------------------

What we’ll do in this chapter
• get an overview of how state machines work
• model application rules as states and transitions
• build a state machine to embody the rules

Handling state is an important topic in web development these days. We’re
seeing changing ideas and practices in both the front- and back-end worlds.
It’s time we talk more directly about how to manage state in an Elixir project.

The BEAM’s concurrency and fault tolerance bring truly stateful web applica-
tions within reach. But stateful applications bring their own challenges.
Managing state over time requires great care and coordination. Keeping code
clean in the process provides an extra level of difficulty.

We’ll meet these challenges with a purely functional state machine. We’ll see
how to use a data structure and multiple clauses of a single function to make
decisions and enforce rules in an application.

Our state machine will help us coordinate events and transitions as well.

** Most importantly, we’ll keep our code clean by separating state management
from business logic. **

Our first step will be to think a little bit about what state really means.

A Quick Look at State

Holding state is an act of remembering. What we’re remembering is the data
that models our system. We especially care about the transformation of that
data resulting from actions taken in the system over time. The way we
remember is to commit the data to memory on the host machine.

The reason we save state is so future actions can be consistent with the past.
Say we created a new user profile in an application. If the user wants to change
his email address later on, we better still have access to his original profile.

Most web applications present a twist to this story.
HTTP is a stateless protocol. It is specifically designed not to remember 
anything about requests - actions in the system — as soon as they are fulfilled.

pg 44:
As web developers, we typically get around this by storing state in a database.
Since we’re not using a database, we need another place to store state data
over time. In Islands, we’ll store it in long-running Elixir processes, specifically
GenServer processes.

In Chapter 2, Model Data and Behavior, on page 9, we developed the data
to model the domain for Islands. We also wrote functions to transform that
data. This is where we currently stand; we have data, but it is not yet state.

In Chapter 4, Wrap It Up in a GenServer, on page 65, we’ll see how to hold
that data and its subsequent transformations in GenServer processes. This act
of holding data in a process over time will transform the data structures we
now have into state.

Before we move on to where we’re going, though, let’s see where we’ve been.

pg 44:
A Bit of History

Early client-server applications were stateful. Clients connected to the server
and stayed connected while they passed messages back and forth. Think
mainframe applications with dedicated terminals as clients.

That worked well, but it meant that the number of possible clients was limited
by system resources like memory, CPU, and the number of concurrent pro-
cesses the system could support.

The web gets around these limitations because of the nature of HTTP. When
a client makes an HTTP request to a server, it must supply all the data the
server will need to fulfill that request. Once the server sends its response, it
forgets everything it just knew about both the request and the client.

This request-response cycle has been critical to the success and scaling of
the web. It requires fewer system resources to handle vastly more requests
because the server doesn’t need to keep track of anything once it sends a
response. This allows applications to use less expensive shared pools for
resources like database or mainframe connections instead of more expensive
dedicated resources for each client. Applications can manage other resources
like threads and memory the same way.

pg 45:
HTTP shed resource costs, but it picked up others along the way. It’s
impractical to pass all the state a complex application needs to do its work.
Instead, servers store that state in a database, and clients pass along only
enough information for the server to fetch that data to fulfill the request. If
the request involves any change in state, the server needs to write those
changes back to the database. These trips to and from the database add
latency. 

    *** Modeling a domain for a database adds unnecessary complexity. ***

** As developers, we pick up the tab for these added costs in terms of extra code
to write and maintain as well as extra cognitive load when reasoning about
our applications. **

Change Is Afoot

As applications grow and traffic increases, these costs begin to really add up.
At serious scale, they can become prohibitive, so people are looking for ways
to get around them.

We’re at the beginning of a sea change in web development. We’re seeing the
return of stateful servers with persistent client connections. Modern hardware
provides abundant system resources. Elixir provides more than enough
power and concurrency to handle application state and persistent connections
at scale. Phoenix channels make writing those persistent connections a breeze.

With stateful applications, we no longer have the luxury of clean state with
every new request. We have to manage state over time, and make sure it
remains consistent. We need to understand the stages an application can go
through, and handle the transitions between them. We need to ensure that
events in the system are consistent with the stage the application is in.

See: docs/screenshots/managing-state-in-the-front-end-world.png

pg 46:
A Different Path

You might think that we could make decisions about application stages, stage
transitions, and events with conditional logic. You would be right, but the
costs would be high. The number of nested “if” statements necessary to do
the job would lead to a snarl of code paths. Real readability and maintainabil-
ity problems would be our reward.

We’re going to choose a different direction. We’ll implement our own purely
functional state machine to handle all the stages that Islands will go through
in the course of a full game. It’s going to make decisions for the application
about which actions to allow and which to deny at a given stage. It will
manage transitions from one stage to the next, and it will help the game
enforce the rules.

** By implementing our own state machine, we’ll keep the logic for managing
state separate from the rest of the game logic. This separation of concerns
will keep our code clean, readable, and maintainable. **

Before we go any further, let’s get a better understanding of state machines
in general and take a look at an example of a problem that’s ideal for a state
machine to solve.

State Machines

See also:
- google finite state machines
- google Elixir finite state machines
- https://hexdocs.pm/gen_state_machine/GenStateMachine.html
- https://dockyard.com/blog/2020/01/31/state-timeouts-with-gen_statem
- https://codesync.global/media/pretty-state-machine/

State machines are fundamental to computation. A finite state machine is a
form of abstract machine. It defines a finite number of states a system can
be in, as well as any events in the system that can trigger transitions between
those states. Any time we need to model a complex process that proceeds
through a number of states, especially ones that might loop back to earlier
states, we should think about reaching for a finite state machine.

The Meaning of “State”
We need to resolve an ambiguity with the word “state.” In Elixir,
when we say “state,” we mean data held in an Elixir process. In a
state machine, a “state” is the name of a stage an application can
go through. In this chapter, we’ll use “state” to mean the name of
a stage in an application.

There are events in the system that trigger the state transitions.
These are the keys to understanding state machines. Events trigger state
transitions. The state machine may progress to a new state or regress to a
previous one depending on the event and the current state.

pg 47:
A Functional State Machine for Islands

See: lib/islands_engine/rules.ex

Instead, we’ll define a new module, multiple clauses of a single function, and
a data structure to represent the state. Each time we invoke the function,
we’ll pass in the state as well as an event. The function will decide whether
or not to permit that combination of state and event. It will also decide whether
or not to transition to a new state.

If we return a tuple tagged with :ok , that means the combination is permissible.
By returning :error , we signify that it is not. In effect, we’ll be creating a whitelist
of permissible state/event combinations.

pg 48:
Defining the Rules

We’re about to see just how flexible a data structure and a single function
can be. We could use this same pattern to create state machines that fit the
needs of any application we’re working on.

Before we write any code, it’s helpful to have a picture of what we need to
build, what the pieces look like, and how they fit together.

See: docs/screenshots/10-state-machine.png

We’re going to build our state machine one state at a time in the order the
states follow as the game progresses. For each state, we’ll add to the whitelist
of allowable event/state combinations and trigger state transitions where
they are needed.

pg 50:
It’s important to mention that this function doesn’t actually add another player.
It makes a decision about whether it’s okay to add another player based on the
current state of the game. That’s all there is to it.

pg 61:
But we’ve actually done more than that. We’ve created a simple system, sep-
arate from the business logic, that can help manage complex, long-running
processes. We can use the ideas we’ve covered in this chapter anywhere we
need to make decisions about application state.

Wrapping Up

We’ve done a lot of great work in this chapter. We’ve built a state machine
from scratch, encapsulating all the rules of Islands with a data structure and
a handful of clauses for a single function.

Along the way, you learned a lot about finite state machines in general. You
saw how to map events to states in order to make decisions about behavior.
You learned how to manage state and state transitions.

The implementation we came up with is completely decoupled from any of
the other modules we’ve written so far. It can decide whether actions follow
the rules independently, without any knowledge of the application logic for
the rest of the game.


