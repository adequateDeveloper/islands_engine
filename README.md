# IslandsEngine

[Following the book: Functional Web Development with Elixir, OTP, and Phoenix by Lance Halvorsen](https://pragprog.com/book/lhelph/functional-web-development-with-elixir-otp-and-phoenix)

**The Game of Islands**

It’s a game for two players, and each player has a board which consists of a
grid of one hundred coordinates. The grid is labeled with the letters A through
J going down the left side of the board and the numbers 1 through 10 across the
top. We name individual coordinates with a letter-number combination—A1, J5,
D10, and so on.
The players cannot see each other’s boards.

The players have matching sets of islands of various shapes and sizes which
they place on their own boards. The players can move the islands around as
much as they like until they say that they are set. After that, the islands must
stay where they are for the rest of the game.

Once both players have set their islands, they take turns guessing coordinates
on their opponent’s board, trying to find the islands. For every correct guess,
we plant a palm tree on the island at that coordinate. When all the coordinates
for an island have palm trees, the island is forested.

The first player to forest all their opponent’s islands is the winner.

![alt text](docs/screenshots/game-boards.png)

This shows what each player would see as they play. On the left is a view of
their own board where they place their islands. The coordinates that make
up the islands are the color of sand. When the player’s opponent guesses
correctly and hits an island, the coordinate they hit will turn green. If all the
coordinates that make up an island are hit, the island is forested, and when
all of a player’s islands are forested, their opponent has won the game.

On the right is a view of the opponent’s board. This is where the player will
guess coordinates by clicking on them. If a guess hits an island, that coordinate 
will turn green. Otherwise it will turn black.

## Extended Features
* [Splitting APIs, Servers, and Implementations as described by Dave Thomas (see also: genserver_approach.txt)](https://pragdave.me/blog/2017/07/13/decoupling-interface-and-implementation-in-elixir.html)
* Use of Dynamic Supervisor, Registry, ETS, and PersistentEts
* Lots of tests!
## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `islands_engine` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [{:islands_engine, "~> 0.1.0"}]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/islands_engine](https://hexdocs.pm/islands_engine).
